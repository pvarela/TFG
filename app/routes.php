<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', 'HomeController@getIndexPage');

    Route::get('/canvas', function () {
        return View::make('canvas');
    });

    Route::get('login', function () {
        return View::make('login');
    });

    Route::get('registration', function () {
        return View::make('registration');
    });

    Route::get('academies', 'HomeController@getAcademiesSubject');
    Route::get('language-academies', 'HomeController@getLanguageAcademies');
    Route::get('official-language-schools', 'HomeController@getOfficialLanguageSchools');
    Route::get('dorms', 'HomeController@getDorms');
    Route::get('flats', 'HomeController@getFlats');
    Route::get('housings', 'HomeController@getHousings');

    Route::get('accommodation', function(){
        return View::make('accommodation');
    });

    Route::get('useful-information', function(){
        return View::make('useful-information');
    });

    Route::get('leisure', function(){
        return View::make('leisure');
    });

    Route::get('academies/{id}', 'HomeController@getAcademie');
    Route::get('accommodations/{id}', 'HomeController@getAccommodation');


    Route::get('search', 'HomeController@searchServices');
    Route::post('ajaxSearch', 'HomeController@ajaxSearchServices');
    Route::post('/supplier/{id}/ajaxSearchComment', 'CommentsController@ajaxSearchComment');


// Administración de login
    Route::post('/fb', 'HomeController@loginFacebook');
    Route::post('/google', 'HomeController@loginGoogle');

    Route::get('/fb/', 'HomeController@signUpFacebook');
    Route::get('login/fb', 'LoginFacebookController@login');
    Route::get('login/fb/callback', 'LoginFacebookController@callback');

    Route::get('/google', 'HomeController@signUpGoogle');
    Route::get('login/google', array('as' => 'google', 'uses' => 'LoginGoogleController@loginWithGoogle'));

    Route::post('login', function () {
        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'active' => 1
        );
        $remember = Input::get('remember') == 'yes' ? true : false;
        if (Auth::student()->attempt($credentials, $remember)) {
            if (Auth::student()->check()) {
                return Redirect::to('student/showProfile');
            }
        } else if (Auth::supplier()->attempt($credentials, $remember)) {
            if (Auth::supplier()->check()) {
                return Redirect::to('supplier/showProfile');
            }
        } else {
            $errors = "Your email and/or your password is invalid";
            print "error";
            Redirect::back()->withErrors($errors)->withInput(Input::except('password'));
        }
    });

// Administración de logout

    Route::get('logout', function () {
        if (Auth::student()->check()) {
            Auth::student()->logout();
        } else {
            Auth::supplier()->logout();
        }
        return Redirect::to('/');
    });

    Route::get('user', 'UsersController@newUser');
    Route::get('register/user/{activation}', 'UsersController@activeUser');
    Route::post('user', 'UsersController@storeUser');

    Route::get('supplier', 'SuppliersController@newSupplier');
    Route::get('register/supplier/{activation}', 'SuppliersController@activeSupplier');
    Route::post('supplier', 'SuppliersController@storeSupplier');

//Administración Proveedor de Servicios

    Route::group(array('before' => 'auth'), function () {
        Route::get('student/showProfile', 'UsersController@showProfile');
        Route::get('student/editProfile', 'UsersController@editProfile');
        Route::get('supplier/showProfile', 'SuppliersController@showProfile');
        Route::get('supplier/editProfile', 'SuppliersController@editProfile');
        Route::get('services/new', 'ServicesController@getLabels' );
        Route::get('services/edit', 'ServicesController@editService');
        Route::get('services/delete', 'ServicesController@showDeleteService');
        Route::post('services/delete', 'ServicesController@deleteService');
    });


//Administración de Servicios

    Route::group(array('before' => 'auth'), function () {
        Route::get('services/new', 'ServicesController@getLabels' );
//        Route::get('services/edit', 'ServicesController@editService');
        Route::post('services/edit', 'ServicesController@getFormEditService');
        Route::get('services/delete', 'ServicesController@showDeleteService');
        Route::post('services/delete', 'ServicesController@deleteService');
        Route::get('services/new', 'ServicesController@getLabels' );
//        Route::get('services/edit', 'ServicesController@getLabels');
        Route::post('supplier/services/new', 'ServicesController@storeService');

        Route::get('supplier/id/{id}', array('as' => 'supplier.id', 'uses' => 'SuppliersController@showPageSupplier'));
    });

    //Administración de Clases

    Route::group(array('before' => 'auth'), function () {
        Route::get('lessons/new', 'LessonsController@getServices');
        Route::post('supplier/lessons/new', 'LessonsController@storeLesson');
        Route::get('lessons/edit', 'LessonsController@editLesson');
        Route::post('lessons/edit', 'LessonsController@getFormEditLesson');
        Route::get('lessons/delete', 'LessonsController@showDeleteLesson');
        Route::post('lessons/delete', 'LessonsController@deleteLesson');
    });

    // Administración de comentarios

    Route::group(array('before' => 'auth'), function(){
        Route::post('comments/save/{id}', 'CommentsController@storeComment');
        Route::post('students/apply-info', 'UsersController@applyInfo');
        Route::post('supplier/get-info', 'SuppliersController@getStudentInformation');
    });

//Route dataTable
    Route::resource('supplier-administration', 'SuppliersController');
    Route::get('api/supplier/showProfile', array('as' => 'api.supplier.showProfile', 'uses' => 'SuppliersController@getDatatable'));
});

// Administración de Registro

    Route::post('signup', 'HomeController@signUp');
    Route::post('signup/fb', 'HomeController@signUpFacebook');
    Route::post('signup/google', 'HomeController@signUpGoogle');