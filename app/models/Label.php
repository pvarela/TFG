<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 14/02/15
 * Time: 18:38
 */

class Label extends Eloquent{

    protected $table = 'labels';
    protected $fillable = array('name');

    public function comments(){
        return $this->belongsToMany('Comment', 'comments_has_labels', 'labels_id', 'comments_id');
    }

    public function services(){
        return $this->belongsToMany('Service', 'services_has_labels', 'labels_id', 'services_id');
    }

    public function getName(){
        return $this->name;
    }

    public static function getServicesForALabel($label)
    {
        $aux = DB::table('services_has_labels')->where('labels_id', '=', $label->id)->get();
        $services = array();
        foreach($aux as $id){
            array_push($services, Service::where('id', '=', $id->services_id)->first());
        }

        $services = array_unique($services);
        return $services;
    }
} 