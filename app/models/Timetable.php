<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 14/02/15
 * Time: 19:07
 */

class Timetable extends Eloquent{

    protected $table = 'timetables';
    protected $fillable = array('day_week', 'duration', 'start_time');

    public function lessons(){
        return $this->belongsToMany('Service', 'timetables_has_lessons', 'timetables_id', 'lessons_id');
    }
} 