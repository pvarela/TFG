<?php

use Illuminate\Auth\UserInterface;

class Supplier extends Eloquent implements UserInterface
{

    protected $table = 'service_suppliers';
    protected $fillable = array('name', 'reputation', 'description', 'city', 'email', 'password', 'photo_name', 'remember_token', 'active', 'activation', 'uid_fb', 'uid_google', 'access_token_fb', 'access_token_google');

    public function students()
    {
        return $this->belongsToMany('Student', 'student_supplier', 'service_suppliers_id', 'students_id');
    }

    public function services()
    {
        return $this->hasMany('Service', 'service_suppliers_id');
    }

    public function lessons()
    {
        return $this->hasMany('Lesson', 'services_service_suppliers_id');
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhoto()
    {
        return $this->photo_name;
    }

    public static function validateSupplier($input)
    {
        $response = array();
        if (Input::get('edition') == True) {
            $rules = array(
                'name' => array('required', 'min:3'),
                'photo_name' => array('image', 'max:6000'),
                'description' => array('required', 'max:750'),
                'city' => array('required'),
            );

        } else {
            $rules = array(
                'name' => array('required', 'min:3'),
                'email' => 'required|email|unique:service_suppliers',
                'password' => array('required', 'min:6', 'confirmed'),
                'photo_name' => array('image', 'max:6000', 'required'),
                'description' => array('required', 'max:750'),
                'city' => array('required'),
            );
        }

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response['mensaje'] = $validator;
            $response['error'] = true;

        } else {
            $data = Input::all();
            $name = Input::get('name');

            //Almacenamos la imagen en un directorio y el path en DB
            if ($data['photo_name'] != null) {
                $path = Supplier::storeImage($name);
                $data['photo_name'] = $path;
            } else if (Input::get('edition') == True && $data['photo_name'] == null) {
                $data['photo_name'] = Auth::supplier()->get()->getPhoto();
            }

            if (Input::get('edition') == True) {

                $id_supplier = Auth::supplier()->id();
                $supplier = Supplier::find($id_supplier);
                $supplier->description = $data['description'];
                $supplier->email = $data['email'];
                $supplier->city = $data['city'];
                $supplier->photo_name = $data['photo_name'];
                $supplier->save();
                $response['mensaje'] = 'Usuario modificado correctamente';

            } else {

                //Creamos el link de activación y ciframos la contraseña
                $data['password'] = Hash::make(Input::get('password'));
                $activation = Supplier::generateActivation();
                $data['activation'] = $activation;
                $supplier = Supplier::create($data);

                //Enviamos el correo al usuario
                Supplier::sendEmail($data, Input::get('password'));
                $response['mensaje'] = 'Usuario creado correctamente. Recibirás un correo de confirmación para activar tu cuenta';

            }


            $response['error'] = false;
            $response['data'] = $supplier;

        }

        return $response;

    }

    public static function generateActivation()
    {
        return Str::random(20);
    }

    public static function storeImage($name)
    {
        $image = Input::file('photo_name');
        $filename = $name . "." . date('Y-m-d-h-i-s');
        $path = 'img/suppliers/' . $filename;
        $image->move($path, $filename . "." . $image->guessClientExtension());
        $endPath = Student::dropAccents($path . "/" . $filename . "." . $image->guessClientExtension());
        Log::info($endPath);
        return $endPath;
    }

    public static function sendEmail($data, $password)
    {

        $dataMessage = array(
            'name' => $data['name'],
            'activation' => $data['activation'],
            'password' => $password);

        Mail::send('emails.welcome-supplier', $dataMessage, function ($message) {
            $message->to(Input::get('email'), Input::get('name'))->subject('Confirmación de registro de usuario');
        });
    }

    public static function getSupplierByService($service)
    {
        return $service->supplier()->first();
    }
}