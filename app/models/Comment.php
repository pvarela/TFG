<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 14/02/15
 * Time: 18:35
 */

class Comment extends Eloquent {

    protected $table = 'comments';
    protected $fillable = array('comment', 'mark');

    public function students(){
        return $this->belongsTo('students', 'Estudiante_id');
    }

    public function suppliers(){
        return $this->belongsTo('supplier', 'service_supplier_id');
    }

    public function services(){
        return $this->belongsTo('Service', 'services_id');
    }

    public function labels(){
        return $this->belongsToMany('Label', 'comments_has_labels', 'comments_id', 'labels_id');
    }

    public static function getLabels($comment){
        $labels = array();
        $nameLabels = array();
        $labelsTable = DB::table('comments_has_labels')->where('comments_id', '=', $comment->id)->get();

        foreach ($labelsTable as $label){
            $newLabel = DB::table('labels')->where('id', '=', $label->id)->where('name', '!=', 'male')->where('name', '!=', 'female')->get();
            if (sizeof($newLabel) > 0){
                array_push($labels, $newLabel);
            }
        }

        foreach ($labels as $label){
            foreach ($label as $l) {
                array_push($nameLabels, $l->name);
            }

        }

        return $nameLabels;
    }

    public static function validateComment($input){
        $response = array();

        $rules = array(
            'comment' => array('required', 'min:10', 'max:255')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response['mensaje'] = $validator;
            $response['error'] = true;

        } else {
            $formData = Input::all();
            $commentText = $formData['comment'];
            $studentId = Auth::student()->id();
            $student = Student::where('id', '=', $studentId)->first();

            $comment = new Comment();
            $comment->comment = $commentText;
            $comment->Estudiantes_id = $studentId;
            $comment->service_supplier_id = $input['supplierID'];
            $comment->services_id = $input['serviceID'];
            $comment->mark = 5;
            $comment->save();

            $ids = [];
            $label = new Label();
            $label->name = $student->school_year;
            $label->save();
            array_push($ids, $label->id);

            $label = new Label();
            $label->name = $student->career;
            $label->save();
            array_push($ids, $label->id);

            $label = new Label();
            $label->name = $student->sex;
            $label->save();
            array_push($ids, $label->id);
            $comment->labels()->attach($ids);
            $response['mensaje'] = "Su comentario se ha registrado correctamente";
        }

        $response['error'] = false;

        return $response;
    }

    public static function getUserComment($id){
        $student = Student::where('id', '=', $id)->first();
        return $student;
    }
}
