<?php

// se debe indicar en donde esta la interfaz a implementar
use Illuminate\Auth\UserInterface;

class Student extends Eloquent implements UserInterface
{

    protected $table = 'students';
    protected $fillable = array('name', 'surname', 'email', 'photo_name', 'birthday', 'description', 'sex', 'telephone', 'university', 'career', 'school_year', 'username', 'password', 'remember_token', 'activation', 'uid_facebook', 'uid_google', 'access_token_fb', 'access_token_google');

    public function suppliers()
    {
        return $this->belongsToMany('Supplier', 'student_supplier', 'students_id', 'service_suppliers_id');
    }

    public function comments()
    {
        return $this->hasMany('Comment', 'Estudiantes_id');
    }

    //Métodos de la interfaz UserInterface

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function getPhoto()
    {
        return $this->photo_name;
    }

    public function getBirthday()
    {
        $birthday = $this->birthday;
        Log::info($birthday);
        $dateBirthday = explode(" ", $birthday)[0];
        Log::info($dateBirthday);
        $date = date_create_from_format("Y-m-d", $dateBirthday);
        $date = date_format($date, "d/m/Y");
        Log::info($date);
        return $date;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    //Métodos propios para la validación, creación y login de usuarios
    public static function validateUser($input)
    {
        $response = array();

        if (Input::get('edition') == True) {

            $rules = array(
                'name' => array('required', 'min:3'),
                'surname' => array('required', 'min:3'),
                'photo_name' => array('image', 'max:6000'),
                'birthday' => array('required', 'date_format:"d/m/Y'),
                'description' => array('required', 'max:160'),
                'sex' => 'in:male,female',
                'career' => 'required'
            );
        } else {

            $rules = array(
                'name' => array('required', 'min:3'),
                'surname' => array('required', 'min:3'),
                'email' => array('required', 'email', 'unique:students'),
                'photo_name' => array('image', 'max:6000'),
                'birthday' => array('required', 'date_format:"d/m/Y'),
                'description' => array('required', 'max:160'),
                'sex' => 'in:male,female',
                'career' => 'required'
            );
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response['mensaje'] = $validator;
            $response['error'] = true;

        } else {
            $data = Input::all();

            //Creamos el usuario, la contraseña cifrada y el link de activación
            $username = Student::generateUserName(Input::get('name'), Input::get('surname'));
            $password = Student::generatePassword();
            $birthday = null;
            $activation = null;
            $passwordHash = null;

            if(Input::get('edition') == False){

                //Cambiamos el formato de la fecha para almacenarlo en la base de datos. FORMATO_OBLIGATORIO("Y-m-d")
                $date_old = Input::get('birthday');
                $birthday = date("Y-m-d", strtotime($date_old));
                $data['birthday'] = $birthday;

                $activation = Student::generateActivation();
                $passwordHash = Hash::make($password);
            }

            //Almacenamos la imagen en un directorio y el path en DB
            if ($data['photo_name'] != null) {
                $path = Student::storeImage($username);
                $data['photo_name'] = $path;
                Log::info("Se regresa del metodo de la foto");

            } else if (Input::get('edition') == True && $data['photo_name'] == null) {
                $data['photo_name'] = Auth::student()->get()->getPhoto();
            }

            if (Input::get('edition') == True) {

                $birthday = $data['birthday'];
                $birthday = date_create_from_format("d/m/Y", $birthday);
                $birthday = date_format($birthday, "Y-m-d");

                $id_student = Auth::student()->id();
                $user = Student::find($id_student);
                $user->school_year = $data['school_year'];
                $user->career = $data['career'];
                $user->birthday = $birthday;
                $user->telephone = $data['telephone'];
                $user->university = $data['university'];
                $user->description = $data['description'];
                $user->photo_name = $data['photo_name'];
                $user->save();
                $response['mensaje'] = 'Usuario modificado correctamente';

            } else {

                //Almacenamos el nombre de usuario, la contraseña cifrada y el link de activación en los datos del usuario
                $data['username'] = $username;
                $data['password'] = $passwordHash;
                $data['activation'] = $activation;
                $user = Student::create($data);
                $response['mensaje'] = 'Usuario creado correctamente. Recibirás un correo con tu usuario y contraseña';
                Student::sendEmail($username, $password, $activation);

            }

            $response['error'] = false;
            $response['data'] = $user;

            //Enviamos el correo
        }

        return $response;
    }

    public static function generateUserName($name, $surname)
    {

        //TODO: Faltaría comprobar en la base de datos si el nombre de usuario existe para asignar otro ;)
        $nameClear = Student::dropAccents($name);
        $surnameClear = Student::dropAccents($surname);
        $username = substr($nameClear, 0, 3);
        $surnames = explode(" ", $surnameClear);
        foreach ($surnames as $modifySurname) {
            if (strtolower($modifySurname != "del")) {
                $username .= substr($modifySurname, 0, 3);
            }
        }

        $username = strtolower($username);
        return $username;
    }

    public static function generatePassword()
    {
        $generatePassword = Str::random(12);

        return $generatePassword;
    }

    public static function generateActivation()
    {
        return Str::random(20);
    }

    public static function storeImage($username)
    {
        $image = Input::file('photo_name');
        $filename = $username . "." . date('Y-m-d-h-i-s');
        $path = 'img/users/' . $filename;
        $image->move($path, $filename . "." . $image->guessClientExtension());
        $endPath = Student::dropAccents($path . "/" . $filename . "." . $image->guessClientExtension());
        Log::info($endPath);
        return $endPath;

    }

    //Utils

    public static function dropAccents($incoming_string)
    {
        $tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿ";
        $replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuy";
        return utf8_encode(strtr(utf8_decode($incoming_string),
            utf8_decode($tofind),
            $replac));
    }

    public static function sendEmail($username, $password, $activation)
    {

        $dataMessage = array(
            'name' => Input::get('name'),
            'surname' => Input::get('surname'),
            'username' => $username,
            'password' => $password,
            'activation' => $activation
        );

        Mail::send('emails.welcome', $dataMessage, function ($message) {
            $message->to(Input::get('email'), Input::get('name'))->subject('Confirmación de registro de usuario');
        });
    }

}
