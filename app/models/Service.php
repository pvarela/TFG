<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 1/02/15
 * Time: 13:06
 */

class Service extends Eloquent {

    protected $table = 'services';
    protected $fillable = array('street', 'city', 'description', 'email', 'name', 'number', 'web', 'photo_name', 'reputation', 'telephone');

    public function supplier(){
        return $this->belongsTo('Supplier', 'service_suppliers_id');
    }

    public function lessons(){
        return $this->hasMany('Lesson', 'id');
    }

    public function tariffs(){
        return $this->hasMany('Tariff', 'id');
    }

    public function labels()
    {
        return $this->belongsToMany('Label', 'services_has_labels', 'services_id', 'labels_id');
    }

    public static function getLabelsOfAService($service){
        return DB::table('services_has_labels')->where('services_id', '=', $service->id)->get();
    }

    public static function getLessonsOfAService($service){
        return Lesson::where('services_id', '=', $service->id)->get();
    }

    public static function validateService($input)
    {
        $response = array();

        $rules = array(
            'name' => array('required', 'min:3'),
            'email' => 'required',
            'web' => 'min:5',
            'telephone' => array('digits:9'),
            'photo_name' => array('image', 'max:6000'),
            'description' => array('required', 'max:1000'),
            'city' => array('required', 'min:3'),
            'street' => array('required', 'min:3'),
            'number' => array('numeric'),
            'labels' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){

            $response['mensaje'] = $validator;
            $response['error'] = true;

        }else{

            $data = Input::all();
            $name = $data['name'];

            // Save image path
            $path = Service::storeImage($name);
            $data['photo_name'] = $path;
            Log::info(print_r($data, true));
            $data['reputation'] = 5;

            $service = new Service($data);
            $service->photo_name = $path;
            $supplier = Supplier::find(Auth::supplier()->id());
            $service->supplier()->associate($supplier);

            Log::info(print_r($data,true));
            $labels = $data['labels'];

            foreach($labels as $label)
            {
                $service->save();
                $service->labels()->attach($label);
            }

            $response['mensaje'] = "Servicio creado correctamente";
            $response['data'] = $service;
            $response['error'] = false;
        }

        return $response;
    }

    public static function storeImage($name){
        $image = Input::file('photo_name');
        $filename = $name . "." . date('Y-m-d-h-i-s');
        $path = 'img/suppliers/services/' . $filename;
        $path = Student::dropAccents(str_replace(' ', '_', $path));
        $fileName = Student::dropAccents(str_replace(' ', '_', $filename . "." . $image->guessClientExtension()), $filename);
        $image->move($path, $fileName);
        $endPath = $path . "/" . $filename . "." . $image->guessClientExtension();
        return Student::dropAccents(str_replace(' ', '_', $endPath));
    }

    public static function validateEditService($input){

        $response = array();
        $data = Input::all();

        $rules = array(
            'name' => array('required', 'min:3'),
            'email' => 'required',
            'web' => 'min:5',
            'telephone' => array('digits:9'),
            'photo_name' => array('image', 'max:6000'),
            'description' => array('required', 'max:1000'),
            'city' => array('required', 'min:3'),
            'street' => array('required', 'min:3'),
            'number' => array('numeric'),
            'labels' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){

            $response['mensaje'] = 'El servicio no se ha podido modificar. Inténtalo de nuevo';
            $response['error'] = true;

        }else {

            $service = Service::find($data['service']);
            $service->name = $data['name'];
            $service->email = $data['email'];
            $service->web = $data['web'];

            if ($data['photo_name'] != null) {
                $path = Service::storeImage($data['name']);
                $data['photo_name'] = $path;
            } else{
                $data['photo_name'] = $service->photo_name;
            }

            $service->photo_name = $data['photo_name'];
            $service->description = $data['description'];
            $service->city = $data['city'];
            $service->street = $data['street'];
            $service->number = $data['number'];
            $service->save();

            $labels = $data['labels'];
            Log::info("labels");
            Log::info(print_r($labels, true));

            Log::info(gettype($labels));

            //Quitamos los labels que tenia
            $labelsService = $service->labels()->get();
            $idsLabels = array();

            foreach($labelsService as $labelService){
                array_push($idsLabels, $labelService->id);
            }

            $service->labels()->detach($idsLabels);

            //Asociamos los nuevos
            foreach($labels as $label){
                Log::info(print_r($label,true));
                $service->labels()->attach($label);
            }

            $response['mensaje'] = 'El servicio se ha modificado correctamente';
            $response['error'] = false;
        }

        return $response;
    }

    public static function deleteServices($input)
    {
        $response = array();
        $data = $input;

        $rules = array(
            'services' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){

            $response['mensaje'] = 'No se han podido eliminar el/los servicio/s. Inténtalo de nuevo';
            $response['error'] = true;

        }else {
            Log::info(print_r($data, true));
            foreach ($data as $id) {
                $service = Service::find($id);
                Log::info(print_r($service,true));
                $service->delete();
            }
            $response['mensaje'] = 'Los servicios se han borrado correctamente';
            $response['error'] = false;

            return $response;
        }

    }

    public static function createURL($service)
    {
        $labelsAux = Service::getLabelsOfAService($service);
        $labels = array();
        $labelsName = array();
        foreach($labelsAux as $labelAux){
            array_push($labels, Label::where('id', '=', $labelAux->labels_id)->first());
        }
        $labels = array_unique($labels);

        foreach($labels as $label){
            array_push($labelsName, $label->name);
        }

        $arrayAcademies = array("Inglés", "Francés", "Academias de Idiomas", "Academias", "Escuelas Oficiales de Idiomas", "Academias de Asignaturas");
        $arrayAccommodations = array("Colegios Mayores", "Residencias", "Pisos de alquiler", "Pisos");

        if(count(array_intersect($arrayAcademies, $labelsName)) > 0){
            return url('academies/' . $service->id);
        }else if(count(array_intersect($arrayAccommodations, $labelsName)) >0 ){
            return url('accommodations/'. $service->id);
        }
    }
}