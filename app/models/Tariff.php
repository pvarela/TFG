<?php

class Tariff extends Eloquent{

    protected $table = 'tariffs';
    protected $fillable = array('name', 'description', 'price');

    public function services(){
        return $this->belongsTo('Service', 'services_id');
    }

    public function lessons(){
        return $this->hasMany('Lesson', 'lessons_id');
    }
} 