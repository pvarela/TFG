<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 14/02/15
 * Time: 18:55
 */

class Lesson extends Eloquent{

    protected $table = 'lessons';
    protected $fillable = array('subject', 'level');

    public function service(){
        return $this->belongsTo('Service', 'services_id');
    }

    public function supplier(){
        return $this->belongsTo('Supplier', 'services_service_suppliers_id');
    }

    public function timetables(){
        return $this->belongsToMany('Timetable', 'timetables_has_lessons', 'lessons_id', 'timetables_id');
    }

    public function tariffs(){
        return $this->belongsTo('Tariff', 'tariffs_id');
    }

    public static function getTariffOfALesson($lesson)
    {
        return DB::table('tariffs')->where('id', '=', $lesson->tariffs_id)->first();
    }

    public static function getTimetablesOfALesson($lesson)
    {
        $timetables = DB::table('timetables_has_lessons')->where('lessons_id', '=', $lesson->id)->get();
        $timetablesArray = array();
        foreach($timetables as $timetable){
            array_push($timetablesArray, Timetable::where('id', '=', $timetable->timetables_id)->first());
        }
        $timetablesArray = array_unique($timetablesArray);
        return $timetablesArray;
    }

    // inputStartsWith a string
    public static function inputStartsWith($pattern = null)
    {
        $input = Input::all(); $result = array();
        array_walk($input, function ($v, $k) use ($pattern, &$result) {
            if(starts_with($k, $pattern)) {
                $result[$k] = $v;
            }
        });
        return $result;
    }

    public static function storeImage($name){
        $image = Input::file('photo_name');
        $filename = $name . "." . date('Y-m-d-h-i-s');
        $path = 'img/suppliers/services/' . $filename;
        $path = Student::dropAccents(str_replace(' ', '_', $path));
        $fileName = Student::dropAccents(str_replace(' ', '_', $filename . "." . $image->guessClientExtension()), $filename);
        $image->move($path, $fileName);
        $endPath = $path . "/" . $filename . "." . $image->guessClientExtension();
        return Student::dropAccents(str_replace(' ', '_', $endPath));
    }

    public static function validateLesson($input)
    {
        $response = array();

        $rules = array(
            'subject' => array('required', 'min:3'),
            'level' => array('required', 'min:3'),
            'name-tariff' => array('required', 'min:5'),
            'description' => array('required', 'min:20'),
            'price' => array('required', 'numeric'),
            'description' => array('required', 'max:160'),
            'service' => array('required'),
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){

            $response['mensaje'] = $validator;
            $response['error'] = true;

        }else{

            $data = Input::all();
            $service = Service::find($data['service']);
            Log::info(print_r($data, true));
            $dataLesson = array();
            $dataTariff = array();
            $dataTimetable = array();

            $dataTariff['name'] = $data['name-tariff'];
            $dataTariff['description'] = $data['description'];
            $dataTariff['price'] = $data['price'];
            $dataTariff['services_id'] = $data['service'];
            $tariff = new Tariff($dataTariff);
            $tariff->services()->associate($service);
            $tariff->save();

            $dataLesson['subject'] = $data['subject'];
            $dataLesson['level'] = $data['level'];
            $lesson = new Lesson($dataLesson);
            $lesson->services_service_suppliers_id = Auth::supplier()->get()->id;
            $lesson->services_id = $data['service'];
            $lesson->tariffs()->associate($tariff);
            $lesson->save();


            $timetables = Lesson::inputStartsWith('lesson');
            Log::info(print_r($timetables, true));

            foreach($timetables as $timetable){
                $date = explode(" ", $timetable);
                $dataTimetable['day_week'] = $date[0];
                $dataTimetable['duration'] = $data['duration'];
                $dataTimetable['start_time'] = $date[1];

                $timetable = new Timetable($dataTimetable);
                $timetable->save();
                $timetable->lessons()->attach($lesson->id);

            }

            $response['mensaje'] = "Clase creada correctamente";
            $response['data'] = $lesson;
            $response['error'] = false;
        }

        return $response;
    }

    public static function validateEditLesson($input){

        $response = array();
        $data = Input::all();

        $rules = array(
            'subject' => array('required', 'min:3'),
            'level' => array('required', 'min:3'),
            'name-tariff' => array('required', 'min:5'),
            'description' => array('required', 'min:50'),
            'price' => array('required', 'numeric'),
            'description' => array('required', 'max:160'),
//            'service' => array('required'),
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            Log::info(print_r($validator->errors(),true));
            $response['mensaje'] = 'La clase no se ha podido modificar. Inténtalo de nuevo';
            $response['error'] = true;

        }else {
            $lesson = Lesson::find($data['idLesson']);
            $lesson->subject = $data['subject'];
            $lesson->level = $data['level'];
            $lesson->save();

            $tariff = $lesson->tariffs()->first();
            $tariff = Tariff::find($tariff->id);
            $tariff->name = $data['name-tariff'];
            $tariff->description = $data['description'];
            $tariff->price = $data['price'];
            $tariff->save();

            $timetables = Lesson::inputStartsWith('lesson');
            $oldTimetables = $lesson->timetables()->get();

            foreach($oldTimetables as $oldTimetable){
                $lesson->timetables()->detach($oldTimetable->id);
            }

            foreach($timetables as $timetable){
                log::info(print_r($timetable,true));
                $date = explode(" ", $timetable);
                $datatimetable['day_week'] = $date[0];
                $datatimetable['duration'] = $data['duration'];
                $datatimetable['start_time'] = $date[1];

                $timetable = new timetable($datatimetable);
                $timetable->save();

                $timetable->lessons()->attach($lesson->id);
            }

            $response['mensaje'] = 'La clase se ha modificado correctamente';
            $response['error'] = false;
        }

        return $response;
    }

    public static function deleteLessons($input){
        $response = array();
        $data = $input;

        $rules = array(
            'lessons' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()){

            $response['mensaje'] = 'No se han podido eliminar la/s clase/s. Inténtalo de nuevo';
            $response['error'] = true;

        }else {
            Log::info(print_r($data, true));
            foreach ($data as $id) {
                $lesson = Lesson::where('id', '=', $id)->get();
                Log::info(print_r($lesson,true));
//                $lesson->delete();
            }
            $response['mensaje'] = 'Las clases se han borrado correctamente';
            $response['error'] = false;

            return $response;
        }
    }
}