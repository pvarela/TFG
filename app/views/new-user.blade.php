@extends('layouts.pattern')
@section('content')
@parent
{{ HTML::style('assets/css/sign-up-forms.css') }}
<div class="container">
    <div>
        <h1>Registro
            <small>{{ trans('pattern.estudiantes') }}</small>
        </h1>
    </div>

    @if (Session::has('mensaje'))
    <div class="alert alert-success" role="alert">
        <h4>{{ Session::get('mensaje') }}</h4>
    </div>
    @endif

    {{ Form::open(array('url' => 'user', 'files' => 'true')) }}
    <div class="row">
        <div class="col-md-3">
            <div class="text-center {{$errors->has('photo_name') ? ' has-error' : '' }}">
                <?php isset($student) ? $src = URL::asset($student->photo_name) : $src =  URL::asset('assets/images/interrogation.png') ?>
                {{ HTML::image(URL::asset($src), 'photo_name', array('name' => 'photo_name', 'class' => 'avatar img-circle media-object', 'width' => '125px', 'height' => '125px', 'src' => $src)) }}
                <h6>Upload another photo...</h6>

                <input name="photo_name" class="form-control" type="file">
            </div>
        </div>
        <div class="col-md-9">
            <?php isset($student) ? $disabled = "readonly" : $disabled=""  ?>
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                @if ($errors->has('name'))
                    <label class="control-label">
                        @foreach ($errors->get('name') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-name">{{ trans('forms.nombre') }}</span>
                    {{ Form::text('name', isset($student) ? $student->name : Input::old('name'), array('id' => 'sign-up-student-form','class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-name')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('surname') ? ' has-error' : '' }}">
                @if ($errors->has('surname'))
                    <label class="control-label">
                        @foreach ($errors->get('surname') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-surname">{{ trans('forms.apellidos') }}</span>
                    {{ Form::text('surname', isset($student) ? $student->surname : Input::old('surname'), array('class' => 'form-control', 'aria-describedby' => 'basic-addon-surname')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                @if ($errors->has('email'))
                    <label class="control-label">
                        @foreach ($errors->get('email') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-email">{{ trans('forms.correo') }}</span>
                    {{ Form::email('email', isset($student) ? $student->email : Input::old('email'), array('class' => 'form-control', 'aria-describedby' => 'basic-addon-email')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
                @if ($errors->has('telephone'))
                    <label class="control-label">
                        @foreach ($errors->get('telephone') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-telephone">{{ trans('forms.telephone') }}</span>
                    {{ Form::text('telephone', isset($student) ? $student->telephone : Input::old('telephone'), array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('birthday') ? ' has-error' : '' }}">
                @if ($errors->has('birthday'))
                    <label class="control-label">
                        @foreach ($errors->get('birthday') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-birthday">{{ trans('forms.fecha-nacimiento') }}</span>
                    {{ Form::text('birthday', isset($student) ? $student->getBirthday() : Input::old('birthday'), array('id' => 'calendar', 'class' => 'form-control',
                    'placeholder' => 'Escoge tu fecha de nacimiento')) }}
                </div>
            </div>
            <?php isset($student) ? $sex = $student->sex : $sex= '' ?>
            <?php $femaleSelected = false; $maleSelected = false ;?>
            <?php isset($sex) and $sex == "male" ? $maleSelected == true : $femaleSelected = true ?>
            <div class="form-group {{ $errors->has('sex') ? ' has-error' : '' }}">
                @if ($errors->has('sex'))
                    <label class="control-label">
                        @foreach ($errors->get('sex') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-sex">{{ trans('forms.sexo') }}</span>
                    <label class="radio-inline">{{ Form::radio('sex', 'male', $maleSelected, ['class' => 'radio-inline']) }}Hombre</label>
                    <label class="radio-inline"> {{ Form::radio('sex', 'female', $femaleSelected, ['class' => 'radio-inline']) }}Mujer</label>
                </div>
            </div>
            <div class="form-group {{ $errors->has('university') ? 'has-error' : '' }}">
                @if ($errors->has('university'))
                    <label class="control-label">
                        @foreach ($errors->get('university') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-university">{{ trans('forms.universidad') }}</span>
                    {{ Form::select('university', array(
                    'Universidad' => array('Universidad de Sevilla' => 'Universidad de Sevilla', 'Universidad Pablo de Olavide' => 'Universidad Pablo de Olavide')
                    ), isset($student) ? $student->university : Input::old('university'), array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('career') ? ' has-error' : '' }}">
                @if ($errors->has('career'))
                    <label class="control-label">
                        @foreach ($errors->get('career') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-career">{{ trans('forms.estudios') }}</span>
                    {{ Form::select('career', array(
                    'Grados' => array('Grado en Ingeniería Informática - Tecnologías Informáticas' => 'Grado en Ingeniería
                    Informática - Tecnologías Informáticas', 'Grado en Ingeniería Informática - Ingeniería del Software' => 'Grado
                    en Ingeniería Informática - Ingeniería del Software')
                    ), isset($student) ? $student->career : Input::old('career'), array('class' => 'form-control')) }}

                </div>
            </div>
            <div class="form-group {{ $errors->has('school_year') ? 'has-error' : '' }}">
                @if ($errors->has('school_year'))
                    <label class="control-label">
                        @foreach ($errors->get('school_year') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-schoolYear">{{ trans('forms.curso') }}</span>
                    {{ Form::select('school_year', array(
                    'Curso' => array('1' => 'Primero', '2' => 'Segundo', '3' => 'Tercero', '4' => 'Cuarto', '5' => 'Quinto')
                    ), isset($student) ? $student->school_year : Input::old('school_year'), array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                @if ($errors->has('description'))
                    <label class="control-label">
                        @foreach ($errors->get('description') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-description">{{ trans('forms.descripcion') }}</span>
                    {{ Form::textarea('description', isset($student) ? $student->description : Input::old('description'), array ('class' => 'form-control', 'rows' => '3', 'maxlength' => '160')) }}
                </div>
            </div>
            <?php isset($student) ? $edition = True : $edition = False ?>
            <div class="form-group">
                <div class="input-group">
                    {{ Form::hidden('edition', $edition) }}
                </div>
            </div>
            <div class="form-group">
                {{Form::submit(Auth::student()->check() ? 'Guardar cambios' : Lang::get('forms.registrar'), array('class' => 'btn btn-success'))}}
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

@stop