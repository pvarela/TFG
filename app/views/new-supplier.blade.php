@extends('layouts.pattern')
@section('content')
@parent
{{ HTML::style('assets/css/sign-up-forms.css') }}
<div class="container">
    <div>
        <h1>Registro
            <small>{{ trans('forms.proveedor-servicios') }}</small>
        </h1>
    </div>

    @if (Session::has('mensaje'))
    <div class="alert alert-success" role="alert">
        <h4>{{ Session::get('mensaje') }}</h4>
    </div>
    @endif

    {{ Form::open(array('id' => 'sign-up-supplier-form', 'url' => 'supplier', 'files' => 'true')) }}
    <!-- left column -->
    <div class="row">
        <div class="col-md-3">
            <div class="text-center {{$errors->has('photo_name') ? ' has-error' : '' }}">
                <?php isset($supplier) ? $src = URL::asset($supplier->photo_name) : $src = URL::asset('assets/images/interrogation.png') ?>
                {{ HTML::image(URL::asset($src), 'photo_name', array('name' => 'photo_name', 'class' => 'avatar img-circle media-object', 'width' => '125px', 'height' => '125px', 'src' => $src)) }}
                <h6>Upload another photo...</h6>

                <input name="photo_name" class="form-control" type="file">
            </div>
        </div>
        <!-- end left column -->
        <div class="col-md-9">
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                @if ($errors->has('name'))
                <label class="control-label">
                    @foreach ($errors->get('name') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <?php isset($supplier) ? $disabled = "readonly" : $disabled="" ?>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-name">{{ trans('forms.nombre-empresa') }}</span>
                    {{ Form::text('name', isset($supplier) ? $supplier->name : Input::old('name'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-name', $disabled)) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                @if ($errors->has('email'))
                <label class="control-label">
                    @foreach ($errors->get('email') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-email">{{ trans('forms.correo') }}</span>
                    {{ Form::email('email', isset($supplier) ? $supplier->email : Input::old('email'), array('class' => 'form-control', 'aria-describedby' => 'basic-addon-email')) }}
                </div>
            </div>
            @if (!Auth::supplier()->check())
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                @if ($errors->has('password'))
                <label class="control-label">
                    @foreach ($errors->get('password') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-password">{{ trans('forms.contraseña') }}</span>
                    {{ Form::password('password', array('class' => 'form-control', 'aria-describedby' => 'basic-addon-password')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                @if ($errors->has('password_confirmation'))
                <label class="control-label">
                    @foreach ($errors->get('password_confirmation') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-password_confirmation">{{ trans('forms.confirma') }} {{ trans('forms.contraseña') }}</span>
                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'aria-describedby' => 'basic-addon-password_confirmation')) }}
                </div>
            </div>
            @endif
            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                @if ($errors->has('city'))
                    <label class="control-label">
                        @foreach ($errors->get('city') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-city">{{ trans('forms.city-supplier') }}</span>
                    {{ Form::text('city', isset($supplier) ? $supplier->city : Input::old('city'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-city')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                @if ($errors->has('description'))
                <label class="control-label">
                    @foreach ($errors->get('description') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-description">{{ trans('forms.descripcion') }}</span>
                    {{ Form::textarea('description', isset($supplier) ? $supplier->description : Input::old('description'), array ('class' => 'form-control', 'rows' => '3', 'maxlength' => '750')) }}
                </div>
            </div>
            <?php isset($supplier) ? $edition = True : $edition = False ?>
            <div class="form-group">
                <div class="input-group">
                    {{ Form::hidden('edition', $edition) }}
                </div>
            </div>
            <div class="form-group">
                {{Form::submit(Auth::supplier()->check() ? 'Guardar cambios' : Lang::get('forms.registrar'), array('class' => 'btn btn-success'))}}
            </div>
        </div>

    </div>
    {{ Form::close() }}
</div>
@stop
@section('scripts')
    @parent
    {{ HTML::script('/assets/js/utils.js') }}
@stop