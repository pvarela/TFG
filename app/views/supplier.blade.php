@extends('layouts.pattern')
@section('content')
    {{ HTML::style('assets/css/suppliers.css', array('media' => 'screen')) }}
    @parent
    <div class="container">
        @if(isset($message))
            <div id="message" class="alert alert-success" role="alert">
                 {{ $message }}
            </div>
        @endif
        <div id="first-row" class="row">
            <div class="col-lg-8 col-xs-12">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @for($i=0; $i<count($services); ++$i)
                            <li data-target="#carousel" data-slide-to="{{$i}}"></li>
                        @endfor
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @for($i=0; $i<count($services); ++$i)
                            @if($i==0)
                                <div class="item active item-slide">
                            @else
                                <div class="item item-slide">
                            @endif
                                    {{ HTML::image($services[$i]->photo_name, $services[$i]->name) }}

                                    <div class="carousel-caption">
                                        <h3>{{ $services[$i]->name }}</h3>
                                        <p>{{ $services[$i]->city }}</p>
                                    </div>
                                </div>
                        @endfor
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div id="apply-box" class="col-lg-4 col-xs-12 sidebar">
                <div class="apply-info">
                    <h4> {{ trans('perfil.solicita-info') }} </h4>
                    <hr>
                    {{ Form::open(array('action' => 'UsersController@applyInfo', 'class' => 'form-inline')) }}
                        <div class="form-group">
                            <div class="input-group input-group-apply">
                               {{ Form::text('date-call', '', array('id' => 'date-call', 'class' => 'apply-input form-control', 'required')) }}
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-apply">
                                {{ Form::select('hour-call',  array(
                                '08:00' => '08:00',
                                '09:00' => '09:00',
                                '10:00' => '10:00',
                                '11:00' => '11:00',
                                '12:00' => '12:00',
                                '13:00' => '13:00',
                                '14:00' => '14:00',
                                '15:00' => '15:00',
                                '16:00' => '16:00',
                                '17:00' => '17:00',
                                '18:00' => '18:00',
                                '19:00' => '19:00',
                                '20:00' => '20:00',
                                '21:00' => '21:00'), '', array('class' => 'apply-input form-control', 'required')) }}
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <div class="input-group">
                                    {{ Form::hidden('emailSupplier', $supplier->email) }}
                                    {{ Form::hidden('supplier', $supplier) }}
                                    {{ Form::hidden('supplierID', $supplier->id)  }}
                                </div>
                            </div>
                            <div class="form-group">
                                @if(!Auth::student()->check())
                                    <p class="text-info">Para solicitar información debe estar conectado</p>
                                @else
                                    {{ Form::submit(Lang::get('perfil.apply-for-call'), array('class' => 'btn btn-success btn-block')) }}
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="second-row" class="row">
            <div class="col-xs-8 col-md-8 col-lg-8">
                <div>
                    <h1> {{ $supplier->name }}
                        <small>{{ trans('perfil.proveedor-servicios') }}</small>
                    </h1>
                </div>
                <hr>
                <div class="col-md-8">
                    <div class="img img-thumbnail img-responsive img-supplier" style="float:left;">
                        <figure>
                            {{ HTML::image(URL::asset($supplier->getPhoto()), 'photo_name', array('class' => 'avatar img media-object')) }}
                        </figure>
                    </div>
                    <div class="info-supplier">
                        {{ $supplier->name }}
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div id="rating-stars"></div>
                </div>
                <div class="clearfix"></div>
                <div class="tab">
                    <ul class="nav nav-tabs tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#description-tab" aria-controls="#description-tab" role="tab" data-toggle="tab">{{ trans('forms.description') }}</a></li>
                        <li role="presentation"><a href="#comments-tab" aria-controls="#comments-tab" role="tab" data-toggle="tab">{{ trans('forms.comments') . "(" . $supplier->reputation . ")" }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="description-tab" role="tabpanel" class="tab-pane fade in active">
                            <div class="tabs-content">
                                <p class="text-info">{{ $supplier->description }}</p>
                                <hr>
                                <h3>
                                    Lugar
                                </h3>
                                <p>{{ $supplier->city }}</p>
                                <a data-toggle="modal" data-target="#map-modal" href="#" data-original-title="" title="">
                                    <button class="btn btn-primary">
                                        Ver en el mapa
                                    </button>
                                </a>
                                <hr>
                                <h3>Servicios</h3>
                                @foreach($services as $service)
                                    <div class="service">
                                        <div class="col-md-5">
                                            <a href="{{ Service::createURL($service) }}">{{ HTML::image($service->photo_name, '', array('class' => 'img img-responsive img-service')) }}</a>
                                        </div>
                                        <div class="col-md-7">
                                            <h4 class="title-block"> {{ $service->name }} </h4>
                                            <p class="text-justify text-center"> {{ $service->description }}</p>
                                        </div>
                                        <br>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="comments-tab" role="tabpanel" class="tab-pane fade">
                            <div class="form form-inline form-comments">
                                {{ Form::open(array('id' => 'search-form')) }}
                                    <div class="form-group col-md-3">
                                        {{ Form::label('option-sex', 'Sexo: ', array('class' => 'label-control')) }}
                                        {{ Form::select('option-sex', array(
                                            'all' => Lang::get('perfil.all'),
                                            'male' => Lang::get('perfil.male'),
                                            'female' => Lang::get('perfil.female')
                                        ), '', array('id' => 'option-sex', 'class' => 'form-control option-comment', 'onchange' => "ajaxSearchComment($supplier->id)")) }}
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{ Form::label('reputation', 'Puntuación', array('class' => 'label-control')) }}
                                        {{ Form::text('reputation', '', array('id' => 'amount-comment', 'class' => 'form-control', 'readonly' => 'readonly')) }}
                                    </div>
                                <div class="clearfix"></div>
                                <div id="slider-range-comment" class="option"></div>
                                {{ Form::close() }}
                            </div>
                            <div class="clearfix"></div>

                            <div id="comments-list">
                                @foreach($comments as $comment)
                                    <?php $student = Comment::getUserComment($comment->Estudiantes_id) ?>
                                    <div id="comment" . {{ $comment->id }} class="comment-item">
                                        <div class="pull-right">{{ setlocale(LC_ALL, 'es'); echo strftime("%d de %B de %Y - %H:%M ", strtotime($comment->created_at)) }}</div>
                                        <div class="student-info" class="col-md-6">
                                            <div id="photo">{{ HTML::image($student->photo_name, '', array('class' => 'img img-responsive img-rounded img-comment')) }}</div>
                                            <div id="username"><h4>{{ $student->username }}</h4></div>
                                        </div>
                                        <div class="comment-content col-md-6">{{ $comment->comment }}</div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                                <div class="pull-right">
                                    {{ $comments->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Map modal -->
    <div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ $supplier->city }}</h4>
                </div>
                <div class="modal-body">
                    <div id="map-canvas" class="col-md-12" style="height: 500px;"></div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <input id="reputation" type="hidden" value="{{$supplier->reputation}}"/>
    <input id="latitude" type="hidden" value="{{$coordenades[0]}}"/>
    <input id="longitude" type="hidden" value="{{$coordenades[1]}}"/>
@stop
@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    {{ HTML::script('/assets/js/perfil.js') }}
@stop