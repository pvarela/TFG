@extends('layouts.pattern')
@section('content')
@parent
<style>
    .image {
        position:relative;
    }
    .image .text {
        position:absolute;
        top:1.5em;
        left:35%;
    }
</style>
<div id ="content" class="row">
    <tr>
        <div class="image col-xs-6">
            <a href="user">{{ HTML::image('assets/images/students_registration.jpg', 'student registration', array('class' =>
            'img-responsive')) }} </a>
            <div class="text">
                <h3 style="color:white;">Soy estudiante</h3>
            </div>
        </div>

    </tr>
    <tr>
        <div class="image col-xs-6">
            <a href="supplier">{{ HTML::image('assets/images/particular_registration.jpg', 'particular registration', array('class' =>
            'img-responsive')) }} </a>
            <div class="text">
                <h3 style="color:white;">Soy particular</h3>
            </div>
        </div>
    </tr>

</div>
@stop