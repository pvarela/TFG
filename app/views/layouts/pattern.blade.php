<!DOCTYPE html>
<html lang="es">
<meta charset="utf-8">
<head>
    <!-- CSS files -->
    @yield('header')
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/jquery-ui.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/home.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/jquery.dataTables.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/dataTables.responsive.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/bootstrap-social.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/font-awesome.min.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/fonts/raleway.css', array('media' => 'screen')) }}
    <!-- end CSS files -->
    <script type="text/javascript">
        var idx = window.location.toString().indexOf("#_=_");
        if (idx > 0) {
            window.location = window.location.toString().substring(0, idx);
        }
    </script>
</head>
<body>
<div id="wrapper">
    <div id="header" class="container">
        <div id="cabecera" class="row">
            <div class="col-md-5 col-lg-5 col-sm-5 vcenter">
                {{ HTML::link('/', trans('pattern.web'), array('class' => 'navbar-brand')) }}
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 vcenter">LOGO</div>
            <div class="col-md-5 col-lg-5 col-sm-5 vcenter">
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::supplier()->check() || Auth::student()->check())
                        <li>{{ HTML::link('', trans('pattern.signup'), array('id' => 'link_sign_up', 'data-toggle' => 'modal', 'data-target' => 'modal_sign_up')) }}
                        </li>
                        <li>{{ HTML::link('logout', trans('pattern.logout'), array('id' => 'link_logout', 'class' => 'links_cabecera')) }}
                        </li>
                    @else
                        <li>{{ HTML::link('', trans('pattern.signup'), array('id' => 'link_sign_up', 'data-toggle' => 'modal', 'data-target' => 'modal_sign_up')) }}
                        </li>
                        <li>{{ HTML::link('', trans('pattern.login'), array('id' => 'link_login', 'data-toggle' => 'modal',
                            'data-target' => 'modal_login')) }}
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <div id="herramientas" class="row">
            @yield('navbar')
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false"
                                data-target="#nabvar"
                                data-toggle="collapse" type="button">
                                    <span class="sr-only">
                                        Toggle navigation
                                    </span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse" aria-expanded="false" data-target="#navbar">
                        <div class="collapse navbar-collapse navbar-right" id="navbar-right">
                            {{ Form::open(array('url' => 'search','method' => 'GET', 'class' => 'navbar-form navbar-left', 'role' => 'search')) }}
                                <div class="form-group has-feedback">
                                    <i class="form-control-feedback glyphicon glyphicon-search" aria-hidden="true"></i>
                                    <input type="search" name="keyword" class="form-control" placeholder="@lang('pattern.buscar')">
                                </div>
                            {{ Form::close() }}
                            <ul id="language-bar-chooser" class="language_bar_chooser navbar-right">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                @if($localeCode == LaravelLocalization::getCurrentLocale())
                                <li class="active">
                                    <img style="display: block" class="flag flag-{{$localeCode}}"/>
                                </li>
                                @elseif($url = LaravelLocalization::getLocalizedURL($localeCode))
                                <li>
                                    <a rel="alternate" hreflang="{{$localeCode}}" href="{{$url}}">
                                        <img style="display: block" class="flag flag-{{$localeCode}}"/>
                                    </a>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        @if (Auth::supplier()->check() || Auth::student()->check())
                            <?php Auth::supplier()->check() ? $name = Auth::supplier()->get()->name : $name = Auth::student()->get()->name ?>
                            <?php Auth::supplier()->check() ? $photoName = Auth::supplier()->get()->getPhoto() : $photoName = Auth::student()->get()->photo_name ?>
                            <div class="navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                         {{ HTML::image($photoName, '', array('class' => 'profile-image img-circle')) }}
                                         {{ $name }} <b
                                            class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php $url = (Auth::supplier()->check() ? 'supplier/showProfile' : 'student/showProfile') ?>
                                        <li>{{ HTML::link( $url, trans('pattern.perfil'), array('role' => 'menu-login-item')) }}</li>
                                        <?php $url = (Auth::supplier()->check() ? 'supplier/editProfile' : 'student/editProfile') ?>
                                        <li>{{ HTML::link( $url, trans('pattern.editarPerfil')) }}</li>
                                        <li>{{ HTML::link('logout', trans('pattern.logout')) }}</li>
                                    </ul>
                                </li>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div id="desplegable-menu">
                <nav id="wrap-menu-navbar" class="navbar navbar-default">
                    <div id="wrap-menu" class="navbar-collapse collapse" aria-expanded="false" data-target="#wrap-menu">
                        <ul class="nav nav-pills nav-justified" role="tablist">
                            <li id="first-element" role="presentation" class="dropdown">
                                @if (Auth::supplier()->check())
                                    <a id="first-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.perfil')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="first-element-title">
                                        <li role="presentation">
                                            {{ HTML::link('supplier/showProfile', trans('pattern.perfil'), array('role' => 'menuitem', 'tabindex' => '-1')) }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link( 'supplier/editProfile', trans('pattern.editarPerfil')) }}
                                        </li>
                                        <li role="presentation">
                                        {{ HTML::link('logout', trans('pattern.logout')) }}
                                        </li>
                                    </ul>
                                @else
                                    <a id="first-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.academias')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="first-element-title">
                                        <li role="presentation">
                                            {{ HTML::link('academies', Lang::get('pattern.asignaturas'), array('role' => 'menuitem', 'tabindex' => '-1'))  }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('language-academies', Lang::get('pattern.idiomas'), array('role' => 'menuitem', 'tabindex' => '-1'))  }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('official-language-schools', Lang::get('pattern.escuelas'), array('role' => 'menuitem', 'tabindex' => '-1'))  }}
                                        </li>
                                    </ul>
                                @endif
                            </li>
                            <li id="second-element" role="presentation" class="dropdown">
                                @if(Auth::supplier()->check())
                                    <a id="second-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.servicios')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="second-element-title">
                                        <li role="presentation">
                                            {{ HTML::link('services/new', trans('pattern.añadir_servicios'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                            {{--<a role="menuitem" tabindex="-1" href="/services/new">@lang('pattern.añadir_servicios')</a>--}}
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="{{ url('services/edit') }}">@lang('pattern.editar_servicios')</a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="{{ url('services/delete') }}">@lang('pattern.eliminar_servicios')</a>
                                        </li>
                                    </ul>
                                @else
                                    <a id="second-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.alojamientos')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="second-element-title">
                                        <li role="presentation">
                                            {{ HTML::link('dorms',trans('pattern.colegios'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('flats',trans('pattern.pisos'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('housings',trans('pattern.residencias'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                    </ul>
                                @endif
                            </li>
                            <li id="third-element" role="presentation" class="dropdown">
                                @if(Auth::supplier()->check())
                                    <a id="third-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.clases')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="third-element-title">
                                        <li role="presentation">
                                            {{ HTML::link('lessons/new',trans('pattern.añadir_cursos'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('lessons/edit',trans('pattern.editar_cursos'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                        <li role="presentation">
                                            {{ HTML::link('lessons/delete',trans('pattern.eliminar_cursos'), array('role' => 'menuitem', 'tabindex' => '-1') ) }}
                                        </li>
                                    </ul>
                                @else
                                    <a id="third-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.info')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="third-element-title">
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.becas')</a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.tarjetas')</a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.universidades')</a>
                                        </li>
                                    </ul>
                                @endif
                            </li>
                            @if(Auth::supplier()->check() != 1)
                                <li id="fourth-element" role="presentation" class="dropdown">
                                    <a id="fourth-element-title" class="dropdown-toggle menu-title" data-toggle="dropdown"
                                       aria-haspopup="true" role="button" aria-expanded="false">@lang('pattern.ocio')</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="fourth-element-title">
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.agencias')</a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.gimnasios')</a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">@lang('pattern.tandem')</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                        <!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
    @section('content')
    @show
    @yield('footer')
    <div id="footer" class="">
        <footer class="footer">
            <div id="footer">
                <div id="footerContent">
                    <div id="leftFooter">© Pedro Varela Llorente 2015</div>
                    <div id="rightFooter">
                        <h5>{{ trans('pattern.siguenos') }}</h5>
                        <a href="{{ url('login/fb') }}"><i id="facebook-footer-square" class="propia fa fa-2x fa-facebook-square"></i></a>
                        <a href="{{ url('login/fb') }}"><i id="twitter-footer-square" class="fa fa-2x fa-twitter-square"></i></a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_login" tabindex="-1" role="dialog" data-toggle="modal" aria-labelledby="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                @if ($errors->has('name'))
                <label class="control-label">
                    @foreach ($errors->get('name') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <h4 class="modal-title" id="modal">@lang('pattern.login')</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => 'login', 'role' => 'form', 'method' => 'post')) }}
                <div class="form-group has-feedback">
                    {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email')) }}
                    <i class="form-control-feedback glyphicon glyphicon-envelope" aria-hidden="true"></i>
                </div>

                <div class="form-group has-feedback">
                    <i class="form-control-feedback glyphicon glyphicon-pencil" aria-hidden="true"></i>
                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                    <!--<input name="password" type="password" class="form-control" placeholder="Password">-->
                </div>
                <div class="form-group">
                    {{ Form::label('remember', trans('pattern.recordar'), array('class' =>
                    'control-label')) }}
                    {{ Form::checkbox('remember', 'yes', true) }}
                    <div class="form-group">
                        {{ HTML::link ('forget', trans('pattern.olvidada'), array('class' => '')) }}
                        {{ Form::submit(trans('pattern.login'), array('class' => 'btn btn-primary pull-right')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <div class="form">
                    {{ Form::open(array('id' => 'facebook-login', 'url' => '/fb', 'role' => 'form', 'method' => 'post')) }}
                    <a role="button" class="btn btn-block btn-social btn-lg btn-facebook" onclick="document.getElementById('facebook-login').submit(); return false;">
                        <i class="fa fa-facebook"></i>
                        <input type="hidden" name="sign-up-facebook-button" value="facebook_button_sign_up"/>
                        {{ trans('pattern.loginWith') }} Facebook
                    </a>
                    {{ Form::close() }}
                </div>
                <br>
                <div class="form">
                    {{ Form::open(array('id' => 'google-login', 'url' => '/google', 'role' => 'form', 'method' => 'post')) }}
                    <a class="btn btn-block btn-social btn-lg btn-google" onclick="document.getElementById('google-login').submit(this); return false;">
                        <i class="fa fa-google-plus"></i>
                        {{ trans('pattern.loginWith') }} Google +
                    </a>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
<!-- Modal Registro -->
<div class="modal fade" id="modal_sign_up" tabindex="-1" role="dialog" data-toggle="modal" aria-labelledby="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                @if ($errors->has('name'))
                <label class="control-label">
                    @foreach ($errors->get('name') as $error)
                    {{ $error }}<br>
                    @endforeach
                </label>
                @endif
                <h4 class="modal-title" id="modal">@lang('pattern.signup')</h4>
            </div>
            <form method="POST" action="{{URL::to('signup')}}" role="form" id="sign-up-form">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="radio">
                            <label><input id="radio-user "type="radio" name="name" value="user" checked/>{{ trans('pattern.iamstudent') }} </label>
                        </div>
                        <div class="radio">
                            <label> <input id="radio-supplier "type="radio" name="name" value="supplier"/> {{ trans('pattern.iamcompany') }} </label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::submit(trans('pattern.signup'), array('id' => 'signup-button-submit', 'name' => 'signup-button-submit', 'class' => 'btn btn-primary pull-right')) }}
                    </div>
                </div>
            </form>
            <br/><br/>
            <div class="modal-footer">
                <div>
                    <form method="POST" action="{{ URL::to('/signup/fb') }}" role="form" id="sign-up-form-facebook">
                        <a role="button" id="facebook_button_sign_up" name="facebook_button_sign_up" class="btn btn-block btn-social btn-lg btn-facebook" onclick='sendFacebook();'>
                            <i class="fa fa-facebook"></i>
                            {{ trans('pattern.signupWith') }} Facebook
                        </a>
                    </form>
                </div>
                <br/>
                <div>
                    <form method="POST" action="{{ URL::to('/signup/google') }}" role="form" id="sign-up-form-google">
                        <a role="button" id="google_button_sign_up" name="google_button_sign_up" class="btn btn-block btn-social btn-lg btn-google" onclick="sendGoogle();">
                            <i class="fa fa-google-plus"></i>
                            {{ trans('pattern.signupWith') }} Google +
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Modal Registro -->
<!-- Javascript files -->
{{ HTML::script('assets/js/jquery-2.1.3.min.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/jquery-ui.js') }}
{{ HTML::script('assets/js/home.js') }}
{{ HTML::script('assets/js/jquery.dataTables.min.js' ) }}
{{ HTML::script('assets/js/dataTables.responsive.min.js' ) }}
<!-- end Javascript files -->

<script>

    function sendFacebook(){
        var kind = $("input[name=name]:checked").val();
        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "name").val(kind);
        $('#sign-up-form-facebook').append($(input));
        $('#sign-up-form-facebook').submit();
    }

    function sendGoogle(){
        var kind = $("input[name=name]:checked").val();
        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "name").val(kind);
        $('#sign-up-form-google').append($(input));
        $('#sign-up-form-google').submit();
        return false;
    }
</script>

@section('scripts')
    {{ HTML::script('assets/js/utils.js') }}
@show
</body>
</html>