@extends('layouts.pattern')
@section('content')
    {{ HTML::style('assets/css/services-information.css') }}
    {{ HTML::style('assets/css/accommodations.css') }}
    @parent
    <div class="container" xmlns="http://www.w3.org/1999/html">
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif
        <div class="service-profile">
            <h2 class="title-block"> {{ $service->name }} </h2>
            <hr>
            <div class="col-md-4">
                {{ Form::image($service->photo_name, '', array('class' => 'img img-responsive')) }}
            </div>
            <div class="col-md-8">
                <p class="text-justify text-center"> {{ $service->description }}</p>
            </div>
            <br>
            <div class="clearfix"></div>
        </div>
        <div class="extra-info">
            <span class="glyphicon glyphicon-info-sign"></span>
            {{ Lang::get('pattern.perfil') }}:
            <span>{{ HTML::link('supplier/id/'. $service->supplier()->first()->id, Lang::get('pattern.acceder')) }}</span>
        </div>
        <div class="comments extra-info">
            <h3><span class="glyphicon glyphicon-comment"></span>
                <span>{{ Lang::get('pattern.comments') }}</span>
            </h3>
            <hr>

            <div id="commets-tab" class="extra-info">
                <div id="comments-list">
                    @foreach($comments as $comment)
                        <?php $student = Comment::getUserComment($comment->Estudiantes_id) ?>
                        <div id="comment" . {{ $comment->id }} class="comment-item">
                            <div class="pull-right">{{ setlocale(LC_ALL, 'es'); echo strftime("%d de %B de %Y - %H:%M ", strtotime($comment->created_at)) }}</div>
                            <div class="student-info" class="col-md-6">
                                <div id="photo">{{ HTML::image($student->photo_name, '', array('class' => 'img img-responsive img-rounded img-comment')) }}</div>
                                <div id="username"><h4>{{ $student->username }}</h4></div>
                            </div>
                            <div class="comment-content col-md-6">{{ $comment->comment }}</div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                    <div class="pull-right">
                        {{ $comments->links() }}
                    </div>
                </div>
                <?php Auth::student()->check() ? $disabled="false" : $disabled="disabled" ?>
                <div id="comments">
                    <div id="comment-box">
                        {{ Form::open(array('url' => '/comments/save/'. $supplier->id, 'class' => 'form-horizontal')) }}
                        <legend>{{ trans('perfil.comments') }}</legend>
                        <div class="form-group">
                            {{ Form::image($supplier->photo_name, '', array('class' => 'img-responsive img-rounded', 'style' => 'float:left; width:2em;')) }}
                            <p><strong>{{ $supplier->name }}</strong></p>
                            {{ Form::textarea('comment', '', array('min' => 10, 'max' => 255, 'class' => 'form-control', 'placeholder' => Lang::get('form.leave-comment'))) }}
                            {{ Form::hidden('supplierID', $supplier->id) }}
                            {{ Form::hidden('serviceID', $service->id) }}
                        </div>
                        {{ Form::submit(Lang::get('form.send'), array('class' => 'btn pull-right btn-primary', $disabled)) }}
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    @parent
@stop