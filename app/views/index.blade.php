@extends('layouts.pattern')
@section('content')
@parent
<div id="content">
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            @for($i=0; $i<count($services); ++$i)
                <li data-target="#carousel" data-slide-to="{{$i}}"></li>
            @endfor
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @for($i=0; $i<count($services); ++$i)
                @if($i==0)
                    <div class="item active item-slide">
                @else
                    <div class="item item-slide">
                @endif
                    {{ HTML::image($services[$i]->photo_name, $services[$i]->name) }}

                    <div class="carousel-caption">
                        <h3>{{ $services[$i]->name }}</h3>
                        <p>{{ $services[$i]->city }}</p>
                    </div>
                </div>
            @endfor
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    <div class="clearfix"></div>
    </br>
    <div class="container">
        <h2>{{ Lang::get('pattern.servicios') }}</h2>
        <hr>
        <?php count($services) < 6 ? $max=count($services)+1 : $max=7?>
            @for($i=1; $i<$max; ++$i)
                @if($i%3==0)
                    <div class="row">
                @endif
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            {{ HTML::image($services[$i-1]->photo_name, $services[$i-1]->name) }}
                            <div class="caption">
                                <h3>{{ $services[$i-1]->name }}</h3>

                                <p>{{ $services[$i-1]->description }}</p>

                                <p class="btn-thumbnail"><a href="{{Service::createURL($services[$i-1])}}" class="btn btn-primary" role="button">{{ Lang::get('pattern.buscar') }}</a></p>
                            </div>
                        </div>
                    </div>
                    @if(($i+3)%3==0)
                        </div>
                    @endif
            @endfor
        </div>
    </div>
</div>
@stop

