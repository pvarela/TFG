<!DOCTYPE html>
<html lang="es">
<head>
    <title>Canvas Universidad 2.0</title>
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    <style>
        [class*="col-"] {
            padding-top: 15px;
            padding-bottom: 15px;
            background-color: #eee;
            background-color: rgba(86, 61, 124, .15);
            border: 1px solid #ddd;
            border: 1px solid rgba(86, 61, 124, .2);
        }

        .subdiv {
            border: 1px solid #ddd;
            border: 1px solid rgba(86, 61, 124, .2);
        }
    </style>
</head>
<body>
<div>
    <div class="row-fluid">
        <div class="col-xs-2" style="height: 30em"><strong>Socios clave</strong><span
                class="glyphicon glyphicon-user"></span>
        </div>
        <div class="col-xs-2" style="height: 30em">

            <div class="subdiv" style="height: 12em"><strong>Actividades clave</strong>
                <ul>
                    <li>Captación de empresas</li>
                    <li>Captación de propietarios de pisos en alquiler</li>
                    <li>Difusión en universidades y campus universitarios</li>
                </ul>
            </div
            <div class="subdiv" style="height: 12em"><span><strong>Recursos clave</strong></span>
                <ul>
                    <li>Desarrollo gratuito</li>
                    <li>Posicionamiento natural gratuito</li>
                    <li>Desarrollo de la app gratuito</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2" style="height: 30em"><strong>Propuestas de valor</strong><span
                class="glyphicon glyphicon-folder-open"></span>

            <p>Ofrecer un producto innovador para los estudiantes universitarios que centralice la información
                interesante a la hora de contratar un servicio desde cualquier dispositivo bajo una misma web y/o
                aplicación móvil. </p>
        </div>
        <div class="col-xs-2" style="height: 30em">
            <div class="subdiv" style="height: 12em"><strong>Relaciones con clientes</strong><span
                    class="glyphicon glyphicon-heart"></span>
                <ul>
                    <li>Patrocinio de las empresas en la plataforma</li>
                </ul>
            </div>
            <div class="subdiv" style="height: 12em"><strong>Canales</strong>
                <ul>
                    <li>Anuncios en campus universitarios</li>
                    <li>Posicionamiento natural (Largo plazo)</li>
                    <li>Difusión oral</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2" style="height: 30em"><strong>Segmentos de clientes</strong>
            <ul>
                <li>Estudiantes</li>
                <li>Academias de idiomas</li>
                <li>Academias de asignaturas</li>
                <li>Gimnasios</li>
                <li>Inmobiliarias</li>
                <li>Propietarios de pisos de alquiler</li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="col-xs-6" style="height: 10em"><strong>Estructura de costes</strong>
            <ul>
                <li>Mantenimiento de la plataforma</li>
                <li>Posicionamiento de la plataforma</li>
            </ul>
        </div>
        <div class="col-xs-6" style="height: 10em"><strong>Fuentes de ingresos</strong><span
                class="glyphicon glyphicon-euro"></span>
            <ul>
                <li>Publicidad</li>
                <li>Vender información de usuarios</li>
                <li>Mantenimiento en la plataforma (Futuro)</li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>