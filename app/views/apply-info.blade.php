@extends('layouts.pattern')
@section('content')
    {{ HTML::style('assets/css/suppliers.css', array('media' => 'screen')) }}
    @parent
    <div class="container">
        <h3>Completa tu solicitud rellenando los siguientes datos</h3>
        <hr>
        <div class="col-md-6 col-lg-6">

        </div>
        <div class="col-md-6 col-lg-6">
            {{ Form::open(array('action' => 'SuppliersController@getStudentInformation', 'class' => 'form-inline')) }}
                <div class="form-group">
                    {{ Form::label('label-name', Lang::get('forms.label-name'), array('class' => 'label-control')) }}
                    {{ Form::text('name', $name, array('class' => 'form-control', 'required')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('label-surname', Lang::get('forms.label-surname'), array('class' => 'label-control')) }}
                    {{ Form::text('surname', $surname, array('class' => 'form-control', 'required')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('label-phone', Lang::get('forms.label-phone'), array('class' => 'label-control')) }}
                    {{ Form::text('phone', $telephone, array('class' => 'form-control', 'required')) }}
                </div>
                <div class="form-group">
                    {{ Form::hidden('emailSupplier', $emailSupplier) }}
                    {{ Form::hidden('supplierID', $supplierID) }}
                    {{ Form::hidden('supplier', $supplier) }}
                    {{ Form::hidden('dateCall', $dateCall) }}
                    {{ Form::hidden('hourCall', $hourCall)}}
                    {{ Form::submit(Lang::get('forms.confirm-apply', array('class' => 'btn btn-primary btn-block'))) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
    @stop