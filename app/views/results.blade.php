@extends('layouts.pattern')
@section('content')
    {{ HTML::style('/assets/css/search.css') }}
@parent
    <div id="search-block" class="col-md-2 block">
        <div class="title-block">
            <h3>{{ Lang::get('search.search') }}</h3>
        </div>
        <div id="search-form">
            {{ Form::open(array('id' => 'search-form', 'url' => '', 'action' => '#')) }}
            <fieldset>
                <legend>{{ Lang::get('search.city') }}</legend>
                <div class="form-group">
                    @foreach($cities as $city)
                    {{ Form::checkbox('city[]', $city['city'], null, array('class' => 'option', 'onchange' => 'ajaxSearch()')) }}
                    {{ Form::label($city['city'], '', array('class' => 'label-control')) }}
                    <div class="clearfix"></div>
                    @endforeach
                </div>
            </fieldset>
            <fieldset>
                <legend>{{ Lang::get('search.reputation') }}</legend>
                <div class="form-group">
                    {{ Form::text('reputation', '', array('id' => 'amount', 'readonly' => 'readonly')) }}
                </div>
                <div id="slider-range" class="option"></div>
            </fieldset>
            <fieldset>
                <legend> {{Lang::get('search.label')}} </legend>
                <div class="form-group">
                    @foreach($labels as $id => $label)
                        {{ Form::checkbox('label[]', $id, null, array('class' => 'option', 'onchange' => 'ajaxSearch()')) }}
                        {{ Form::label($label, '', array('class' => 'label-control')) }}
                        <div class="clearfix"></div>
                    @endforeach
                </div>
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
    <div id="results" class="container">
        <h2>{{ Lang::get('search.services') }}</h2>
        <hr>
        @foreach($services as $service)
            <div class="result col-md-12">
                <?php $supplier = Supplier::getSupplierByService($service) ?>
                <div class="col-md-2">
                    <?php $serviceForURL = Service::createURL(Service::where('id', '=', $service->id)->first()) ?>
                    <a href="{{$serviceForURL}}">{{ HTML::image($service->photo_name, '', array('class' => 'image')) }}</a>
                </div>
                <div class="col-md-8">
                    <h4>{{ $service->name }}</h4>
                    {{ $service->description }}
                </div>
                <div class="clearfix"></div>
                <hr>
            </div>
        @endforeach
    </div>
@stop
@section('scripts')
    {{ HTML::script('/assets/js/search.js') }}
@parent
@stop