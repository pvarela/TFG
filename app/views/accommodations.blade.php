@extends('layouts.pattern')
@section('content')
    {{ HTML::style('assets/css/services-information.css') }}
    @parent
    <div class="container">
        <h2>{{ Lang::get('pattern.accommodations') }}</h2>
        <hr>
        @foreach($services as $service)
            <div class="service col-md-12">
                <h4 class="text-center">{{ HTML::link('accommodations/' . $service->id, $service->name)  }}</h4>
                <div class="image-service col-md-4">
                    <a href="{{ url('accommodations/' . $service->id) }}}}">{{ HTML::image($service->photo_name, '', array('class' => 'img img-responsive image')) }} </a>
                </div>
                <div class="text-service text-justify col-md-8">
                    <p class="text-info">{{ $service->description }}</p>
                </div>
                <div class="clearfix"></div>
                <div class="extra-info">
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    <span>{{ $service->email }}</span>
                </div>
                <div class="clearfix"></div>
                <div class="extra-info">
                    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                    <span>{{ $service->telephone }}</span>
                </div>
                <div class="clearfix"></div>
                <div class="extra-info">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    <span>{{ HTML::link($service->web) }}</span>
                </div>
            </div>
        @endforeach
    </div>
@stop