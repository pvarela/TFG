<h3>trans('editar-servicio')</h3>
<hr>
{{ Form::open(array('id' => 'edit-services-form', 'url' => 'services/edit', 'files' => 'true')) }}
<div class="col-md-3">
    <div class="form-group {{ $errors->has('photo_name') ? ' has-error' : '' }}">
        @if ($errors->has('photo_name'))
            <label class="control-label">
                @foreach ($errors->get('photo_name') as $error)
                    {{ $error }}<br>
                @endforeach
            </label>
        @endif
        <?php isset($service->photo_name) ? $src = URL::asset($service->photo_name) : $src=URL::asset('assets/images/interrogation.png')?>
        <div class="text-center {{$errors->has('photo_name') ? ' has-error' : '' }}">
            {{ HTML::image(URL::asset($src), 'photo_name', array('name' => 'photo_name', 'class' => 'avatar img-circle media-object', 'width' => '125px', 'height' => '125px', 'src' => $src)) }}
            <h6>Upload another photo...</h6>
            <input name="photo_name" class="form-control" type="file">
        </div>
    </div>
</div>
<div class="col-md-9">
    <fieldset>
        <legend>Datos principales</legend>
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                @if ($errors->has('name'))
                    <label class="control-label">
                        @foreach ($errors->get('name') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-name">{{ trans('forms.nombre-servicio') }}</span>
                    {{ Form::text('name', isset($service) ? $service->name : Input::old('name'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-name')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                @if ($errors->has('email'))
                    <label class="control-label">
                        @foreach ($errors->get('email') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-email">{{ trans('forms.email-servicio') }}</span>
                    {{ Form::text('email', isset($service) ? $service->email : Input::old('email'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-email')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                @if ($errors->has('telephone'))
                    <label class="control-label">
                        @foreach ($errors->get('telephone') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-telephone">{{ trans('forms.telephone-servicio') }}</span>
                    {{ Form::text('telephone', isset($service) ? $service->telephone : Input::old('telephone'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-telephone')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('web') ? ' has-error' : '' }}">
                @if ($errors->has('web'))
                    <label class="control-label">
                        @foreach ($errors->get('web') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-web">{{ trans('forms.web-servicio') }}</span>
                    {{ Form::text('web', isset($service) ? $service->web : Input::old('web'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-web')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                @if ($errors->has('description'))
                    <label class="control-label">
                        @foreach ($errors->get('description') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-description">{{ trans('forms.description-servicio') }}</span>
                    {{ Form::textarea('description', isset($service) ? $service->description : Input::old('description'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-description')) }}
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Dirección</legend>
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                @if ($errors->has('city'))
                    <label class="control-label">
                        @foreach ($errors->get('city') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-city">{{ trans('forms.city-servicio') }}</span>
                    {{ Form::text('city', isset($service) ? $service->city : Input::old('city'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-city')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('street') ? ' has-error' : '' }}">
                @if ($errors->has('name'))
                    <label class="control-label">
                        @foreach ($errors->get('street') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-street">{{ trans('forms.street-servicio') }}</span>
                    {{ Form::text('street', isset($service) ? $service->street : Input::old('street'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-street')) }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                @if ($errors->has('number'))
                    <label class="control-label">
                        @foreach ($errors->get('number') as $error)
                            {{ $error }}<br>
                        @endforeach
                    </label>
                @endif
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-number">{{ trans('forms.number-servicio') }}</span>
                    {{ Form::text('number', isset($service) ? $service->number : Input::old('number'), array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-number')) }}
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Etiquetas</legend>
        <div class="col-md-12">
            <div class="form-group">
                <div id="labels-group" class="input-group">
                    <span class="input-group-addon" id="basic-addon-label">{{ trans('forms.labels-servicio') }}</span>
                    {{ Form::select('labels[]', $labels, $labelsSelected, array('multiple', 'class' => 'form-control select-toggle', 'aria-describedby' => 'basic-addon-label')) }}
                </div>
            </div>
        </div>
    </fieldset>
    {{ Form::hidden('service', $service->id) }}
</div>
{{ Form::submit('Guardar cambios', array('id' => 'btn-edit-service', 'class' => 'btn btn-success btn-block btn-md pull-right')) }}
{{ Form::close() }}
{{ HTML::script('assets/js/utils.js') }}
{{ HTML::script('assets/js/services.js') }}