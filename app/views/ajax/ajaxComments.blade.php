<div id="comments-list">
    @foreach($comments as $comment)
        <?php $student = Comment::getUserComment($comment->Estudiantes_id) ?>
        <div id="comment" . {{ $comment->id }} class="comment-item">
            <div class="pull-right">{{ setlocale(LC_ALL, 'es'); echo strftime("%d de %B de %Y - %H:%M ", strtotime($comment->created_at)) }}</div>
            <div class="student-info" class="col-md-6">
                <div id="photo">{{ HTML::image($student->photo_name, '', array('class' => 'img img-responsive img-rounded img-comment')) }}</div>
                <div id="username"><h4>{{ $student->username }}</h4></div>
            </div>
            <div class="comment-content col-md-6">{{ $comment->comment }}</div>
            <div class="clearfix"></div>
        </div>
    @endforeach
</div>