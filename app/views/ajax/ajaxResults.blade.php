<h2>{{ Lang::get('search.services') }}</h2>
<hr>
@foreach($services as $service)
    <div class="result col-md-12">
        <?php $supplier = Supplier::getSupplierByService($service) ?>
        <div class="col-md-2">
            <a href="{{Service::createURL(Service::where('id', '=', $service->id)->first())}}"> {{ HTML::image($service->photo_name, '', array('class' => 'image')) }} </a>
        </div>
        <div class="col-md-8">
            <h4>{{ $service->name }}</h4>
            {{ $service->description }}
        </div>
        <div class="clearfix"></div>
        <hr>
    </div>
@endforeach