<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::script('assets/js/jquery-2.1.3.min.js') }}
    {{ HTML::script('assets/js/bootstrap.min.js') }}
</head>
    <title>Nuevo estudiante</title>
</head>
<body>
        <h3>Nuevo estudiante</h3>
        <div class="col-md-6 col-lg-6">
            <p class="text-justify text-info">Un estudiante se ha interesado por tu servicio. Sus datos personales son los siguientes:</p>
            <table class="table table-responsive" style="float: left;">
                <thead>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Fecha de nacimiento</th>
                    <th>Universidad</th>
                    <th>T�tulo Universitario</th>
                </thead>
                <tbody>
                    <td>{{$name}}</td>
                    <td>{{$surname}}</td>
                    <td>{{$email}}</td>
                    <td>{{$birthday}}</td>
                    <td>{{$university}}</td>
                    <td>{{$career}}</td>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 col-lg-6">
            {{HTML::image($photo_name, 'foto estudiante', array('class' => 'img img-thumbnail', 'style' => 'float:right;'))}}
        </div>
        <div class="col-md-12">
            <p class="text-justify text-info">El estudiante desea ser llamado el d�a {{ $dateCall }} a las {{ $hourCall }}</p>
        </div>
</body>
</html>