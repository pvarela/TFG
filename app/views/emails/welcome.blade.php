<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::script('assets/js/jquery-2.1.3.min.js') }}
    {{ HTML::script('assets/js/bootstrap.min.js') }}
</head>
<body>


<div>
    <p class="text-info"> Hola, {{ $name }} {{ $surname }}</p>
    <p class="text-info">Tu nombre de usuario es: {{ $username }}</p>
    <p class="text-info">Tu contraseña es: {{ $password }}</p>
    <p class="text-info">Para activar tu cuenta, haz clic en el siguiente enlace: {{ URL::to('register/user/' . $activation) }}.<br/></p>
</div>

</body>
</html>
