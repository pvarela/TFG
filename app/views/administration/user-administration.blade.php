@extends('layouts.pattern')
@section('content')
    {{ HTML::style('assets/css/suppliers.css', array('media' => 'screen')) }}
    @parent
    <?php $student = Auth::student()->get() ?>
    <div id="content" class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif
        <div class="">
            <h1> {{ $student->name }}
                <small>{{ $student->surname }}</small>
            </h1>
        </div>
        <div>
            <div id="student-photo" style="float:left;">
                {{ HTML::image(Auth::student()->get()->getPhoto(), 'logo_student', array('class' => 'img-thumbnail',
                'width' => '350em', 'id' => 'profile-photo' )) }}
            </div>
            <div id="student-description">
                <p class="text-info">{{ $student->description }}</p>
            </div>
        </div>

        <div class="clearfix"></div>
        {{ $student->university }} en el curso {{ $student->school_year }} de {{ $student->career }}
        <div>
            <h1>{{trans('pattern.lista')}}
                <small>{{Lang::get('pattern.servicios')}}</small>
            </h1>
            <p class="text-justify text-center">{{ Lang::get('pattern.solicitud') }}</p>
            @foreach($services as $service)
               <a href="{{$service[0]->web}}">{{ $service[0]->name }}</a>
            @endforeach
        </div>
        <div>
            <h1>{{ trans('pattern.lista') }}
                <small>{{ Lang::get('pattern.comments') }}</small>
            </h1>
            <p class="text-justify text-center">{{ Lang::get('pattern.info-comments') }}</p>
            @foreach( $comments as $comment)
                <div id="comment" . {{ $comment->id }} class="comment-item">
                    <div class="pull-right">{{ setlocale(LC_ALL, 'es'); echo strftime("%d de %B de %Y - %H:%M ", strtotime($comment->created_at)) }}</div>
                    <div class="student-info" class="col-md-6">
                        <div id="photo">{{ HTML::image($student->photo_name, '', array('class' => 'img img-responsive img-rounded img-comment')) }}</div>
                        <div id="username"><h4>{{ $student->username }}</h4></div>
                    </div>
                    <div class="comment-content col-md-6">{{ $comment->comment }}</div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>
    </div>
@stop
{{ HTML::script('assets/js/jquery-2.1.3.min.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/jquery.dataTables.min.js' ) }}
{{ HTML::script('assets/js/dataTables.responsive.min.js' ) }}
{{ HTML::script('assets/js/utils.js') }}
{{ HTML::script('//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json') }}
