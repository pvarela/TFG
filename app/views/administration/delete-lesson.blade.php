@extends('layouts.pattern')
@section('content')
    @parent
    {{ HTML::style('assets/css/services.css') }}
    <div class="container">
        <h3>{{  Lang::get('perfil.lessons') }}</h3>
        <hr>
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif
        <div class="form form-horizontal">
            {{ Form::open(array('id' => 'delete-lessons-form', 'url' => 'lessons/delete')) }}
            <legend> {{ Lang::get('forms.delete-lessons') }}</legend>
            <fieldset>
                <h4><p class="text-justify text-center">{{ Lang::get('forms.select-lessons') }}</p></h4>
                <div class="checkbox">
                    <label>
                        <input id="all" type="checkbox" onclick="selectAll(this)"/> {{ Lang::get('forms-all') }}
                    </label>
                </div>
                @foreach($lessons as $lesson)
                    <div class="checkbox">
                        <label>
                            <input value="{{ $lesson->id }}" name="lessons[]" type="checkbox" /> {{ $lesson->subject . " " . $lesson->level }}
                        </label>
                    </div>
                @endforeach
                {{ Form::submit(Lang::get('forms.delete-lesson'), array('class' => 'btn btn-success btn-block btn-lg-block')) }}
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('scripts')
    @parent
    {{ HTML::script('assets/js/lessons.js') }}
@stop
