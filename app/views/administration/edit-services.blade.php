@extends('layouts.pattern')
@section('content')
    @parent
    {{ HTML::style('assets/css/services.css') }}
    <div class="container">
        @if (Session::has('mensaje-error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('mensaje-error') }}</h4>
            </div>
        @endif
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif

        <div class="select-service" style="z-index: -10000;">
            <h2>{{ Lang::get('service.edit-service') }}</h2>
            <hr>
            {{ Form::open(array('url' => '')) }}
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon-service">{{ trans('forms.nombre-edit-servicio') }}</span>
                        {{ Form::select('service', $servicesSelect, null, array('class' => 'form-control', 'aria-describedby' =>
                        'basic-addon-service', 'id' => 'select-service')) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div id="edit-services" class="form form-horizontal"></div>
    </div>
@stop
@section('scripts')
    @parent
    {{ HTML::script('/assets/js/utils.js') }}
    {{ HTML::script('/assets/js/services.js') }}
@stop