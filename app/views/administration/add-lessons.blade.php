@extends('layouts.pattern')
@section('content')
    @parent
    {{ HTML::style('assets/css/services.css') }}
    <div class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif
        <div id="add-services" class="form form-horizontal">
            <h3>trans('añadir-clase')</h3>
            <hr>
            {{ Form::open(array('id' => 'add-lessons-form', 'url' => 'supplier/lessons/new', 'files' => 'true')) }}
            <fieldset>
                <legend>Datos principales</legend>
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                        @if ($errors->has('subject'))
                            <label class="control-label">
                                @foreach ($errors->get('subject') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($lesson) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-subject">{{ trans('forms.subject-lesson') }}</span>
                            {{ Form::text('subject', isset($lesson) ? $lesson->subject : Input::old('subject'), array('class' => 'form-control', 'aria-describedby' =>
                            'basic-addon-subject', $disabled)) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('level') ? ' has-error' : '' }}">
                        @if ($errors->has('level'))
                            <label class="control-label">
                                @foreach ($errors->get('level') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($lesson) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-level">{{ trans('forms.level-lesson') }}</span>
                            {{ Form::text('level', isset($lesson) ? $lesson->level : Input::old('level'), array('class' => 'form-control', 'aria-describedby' =>
                            'basic-addon-level', $disabled)) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('service') ? ' has-error' : '' }}">
                        @if ($errors->has('service'))
                            <label class="control-label">
                                @foreach ($errors->get('service') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($service) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-service">{{ trans('forms.subject-service') }}</span>
                            {{ Form::select('service', $services, '', array('class' => 'form-control', 'required')) }}
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Tarifas</legend>
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('name-tariff') ? ' has-error' : '' }}">
                        @if ($errors->has('name-tariff'))
                            <label class="control-label">
                                @foreach ($errors->get('name-tariff') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($tariff) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-name-tariff">{{ trans('forms.name-tariff') }}</span>
                            {{ Form::text('name-tariff', isset($tariff) ? $tariff->name : Input::old('name-tariff'), array('class' => 'form-control', 'aria-describedby' =>
                            'basic-addon-name-tariff', $disabled)) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        @if ($errors->has('description'))
                            <label class="control-label">
                                @foreach ($errors->get('description') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($tariff) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-description">{{ trans('forms.description-tariff') }}</span>
                            {{ Form::text('description', isset($tariff) ? $tariff->street : Input::old('description'), array('class' => 'form-control', 'aria-describedby' =>
                            'basic-addon-description', $disabled)) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                        @if ($errors->has('price'))
                            <label class="control-label">
                                @foreach ($errors->get('price') as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </label>
                        @endif
                        <?php isset($tariff) ? $disabled = "readonly" : $disabled="" ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon-price">{{ trans('forms.price-tariff') }}</span>
                            {{ Form::text('price', isset($tariff) ? $tariff->price : Input::old('price'), array('class' => 'form-control', 'aria-describedby' =>
                            'basic-addon-price', $disabled)) }}
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Horarios</legend>
                <div id="timetables-form" class="form form-horizontal">
                    {{ Form::open(array('url' => '')) }}
                        <div class="form-group col-xs-7 col-md-7 col-lg-7">
                            {{ Form::label(Lang::get('forms.choose-day'), '', array('class' => 'label-control')) }}
                            {{ Form::select('dow', array(
                            'monday' => Lang::get('forms.monday'),
                            'tuesday' => Lang::get('forms.tuesday'),
                            'wednesday' => Lang::get('forms.wednesday'),
                            'thurday' => Lang::get('forms.thursday'),
                            'friday' => Lang::get('forms.friday'),
                            'saturday' => Lang::get('forms.saturday'),
                            'sunday' => Lang::get('forms.sunday')), 'monday', array('id' => 'dow', 'class' => 'form-control')) }}
                        </div>
                        <div class="form-group col-xs-3 col-md-3 col-lg-3">
                            {{ Form::label(Lang::get('forms.choose-number'), '', array('class' => 'label-control')) }}
                            {{ Form::selectRange('nol', '1', '10', '', array('id' => 'nol', 'class' => 'form-control')) }}
                        </div>
                        <br>
                        <div class="form-group col-xs-2 col-md-2 col-lg-2">
                            {{ Form::button('Elegir horario', array('id' => 'btn-choose-timetable', 'class' => 'btn btn-info pull-right', 'data-toggle' => 'modal', 'data-target' => '#classModal')) }}
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-10 col-xs-10 col-lg-10">
                            {{ Form::label(Lang::get('forms.duration'), '', array('class' => 'label-control'))  }}
                            {{ Form::select('duration',  array(
                               '15' => '15 minutos',
                               '30' => '30 minutos',
                               '45' => '45 minutos',
                               '60' => '60 minutos',
                               '75' => '75 minutos',
                               '90' => '90 minutos',
                               '105' => '105 minutos',
                               '120' => '120 minutos'), '', array('class' => 'form-control', 'required')) }}
                        </div>
                    {{ Form::close() }}
                </div>
                {{ Form::submit('Guardar', array('class' => 'btn btn-success btn-block btn-md pull-right')) }}
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
    <!-- Modal classes-->
    <div class="modal fade" id="classModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ Lang::get('forms.choose-timetable') }} </h4>
                </div>
                <div id="form-container" class="form form-horizontal">
                    {{ Form::open() }}
                <div class="modal-body">
                        <div class="timetable-lessons">
                            <div id="lesson1" class="form-group">
                                {{ Form::label(Lang::get('Hora: ', array('class' => 'label-control'))) }}
                                <div class="input-group">
                                    {{ Form::select('time-lesson-1',  array(
                                    '08:00' => '08:00',
                                    '08:30' => '08:30',
                                    '09:00' => '09:00',
                                    '09:30' => '09:30',
                                    '10:00' => '10:00',
                                    '10:30' => '10:30',
                                    '11:00' => '11:00',
                                    '11:30' => '11:30',
                                    '12:00' => '12:00',
                                    '12:30' => '12:30',
                                    '13:00' => '13:00',
                                    '13:30' => '13:30',
                                    '14:00' => '14:00',
                                    '14:30' => '14:30',
                                    '15:00' => '15:00',
                                    '15:30' => '15:30',
                                    '16:00' => '16:00',
                                    '16:30' => '16:30',
                                    '17:00' => '17:00',
                                    '17:30' => '17:30',
                                    '18:00' => '18:00',
                                    '18:30' => '18:30',
                                    '19:00' => '19:00',
                                    '19:30' => '19:30',
                                    '20:00' => '20:00',
                                    '20:30' => '20:30',
                                    '21:00' => '21:00',
                                    '21:30' => '21:30',
                                    '22:00' => '22:00',), '', array('class' => 'form-control', 'required')) }}
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-createTimetable" type="button" class="btn btn-primary">Save changes</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@content('scripts')
@parent
{{ HTML::script('assets/js/utils.js') }}
@stop