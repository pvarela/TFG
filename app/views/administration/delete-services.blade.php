@extends('layouts.pattern')
@section('content')
    @parent
    {{ HTML::style('assets/css/services.css') }}
    <div class="container">
        <h3>{{  Lang::get('perfil.services') }}</h3>
        <hr>
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif
        <div class="form form-horizontal">
            {{ Form::open(array('id' => 'delete-services-form', 'url' => 'services/delete')) }}
            <legend> {{ Lang::get('forms.delete-services') }}</legend>
            <fieldset>
                <h4><p class="text-justify text-center">{{ Lang::get('forms.select-services') }}</p></h4>
                <div class="checkbox">
                    <label>
                        <input id="all" type="checkbox" onclick="selectAll(this)"/> {{ Lang::get('forms-all') }}
                    </label>
                </div>
                @foreach($services as $service)
                    <div class="checkbox">
                        <label>
                            <input value="{{ $service->id }}" name="services[]" type="checkbox" /> {{ $service->name }}
                        </label>
                    </div>
                @endforeach
                {{ Form::submit(Lang::get('forms.delete-service'), array('class' => 'btn btn-success btn-block btn-lg-block')) }}
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
    @stop
@section('scripts')
    @parent
    {{ HTML::script('assets/js/services.js') }}
    @stop
