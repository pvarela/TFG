@extends('layouts.pattern')
@section('content')
@parent
{{ Form::open(array('url' => 'supplier', 'files' => 'true')) }}
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    @if ($errors->has('name'))
    <label class="control-label">
        @foreach ($errors->get('name') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon-name">Nombre de la empresa</span>
        {{ Form::text('name', null, array('class' => 'form-control', 'aria-describedby' =>
        'basic-addon-name')) }}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    @if ($errors->has('email'))
    <label class="control-label">
        @foreach ($errors->get('email') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon-email">Email</span>
        {{ Form::email('email', null, array('class' => 'form-control', 'aria-describedby' => 'basic-addon-email')) }}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
    @if ($errors->has('password'))
    <label class="control-label">
        @foreach ($errors->get('password') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon-password">Contraseña</span>
        {{ Form::password('password', array('class' => 'form-control', 'aria-describedby' => 'basic-addon-password')) }}
    </div>
</div>
<div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    @if ($errors->has('password_confirmation'))
    <label class="control-label">
        @foreach ($errors->get('password_confirmation') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon-password_confirmation">Confirma contraseña</span>
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'aria-describedby' => 'basic-addon-password_confirmation')) }}
    </div>
</div>
<div class="form-group {{ $errors->has('photo') ? ' has-error' : '' }}">
    @if ($errors->has('photo'))
    <label class="control-label">
        @foreach ($errors->get('photo') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">Foto
                {{ Form::file('photo', '') }}
            </span>
        </span>
        {{ Form::text('photo-name','', array('class' => 'form-control', 'readonly')) }}

    </div>
</div>
<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
    @if ($errors->has('description'))
    <label class="control-label">
        @foreach ($errors->get('description') as $error)
        {{ $error }}<br>
        @endforeach
    </label>
    @endif
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon-description">Descripción</span>
        {{ Form::textarea('description', null, array ('class' => 'form-control', 'rows' => '3', 'maxlength' => '160')) }}
    </div>
</div>
<div class="form-group">
    {{Form::submit('Registrar', array('class' => 'btn btn-success'))}}
</div>
{{ Form::close() }}
@stop