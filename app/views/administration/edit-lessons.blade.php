@extends('layouts.pattern')
@section('content')
    @parent
    {{ HTML::style('assets/css/services.css') }}
    <div class="container">
        @if (Session::has('mensaje-error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('mensaje-error') }}</h4>
            </div>
        @endif
        @if (Session::has('mensaje'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('mensaje') }}</h4>
            </div>
        @endif

        <div class="select-lesson" style="z-index: -10000;">
            <h2>{{ Lang::get('lesson.edit-lesson') }}</h2>
            <hr>
            {{ Form::open(array('url' => '')) }}
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon-lesson">{{ trans('forms.nombre-edit-lesson') }}</span>
                    {{ Form::select('lesson', $lessonsSelected, null, array('class' => 'form-control', 'aria-describedby' =>
                    'basic-addon-lesson', 'id' => 'select-lesson')) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <div id="edit-lessons" class="form form-horizontal"></div>
    </div>
@stop
@section('scripts')
    @parent
    {{ HTML::script('/assets/js/utils.js') }}
    {{ HTML::script('/assets/js/lessons.js') }}
@stop