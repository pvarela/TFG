@extends('layouts.pattern')
@section('content')
@parent
<div id="content" class="container">
    @if (Session::has('mensaje'))
    <div class="alert alert-success" role="alert">
        <h4>{{ Session::get('mensaje') }}</h4>
    </div>
    @endif
    <div class="">
        <h1> {{ Auth::supplier()->get()->name }}
            <small>{{trans('pattern.administracion')}}</small>
        </h1>
    </div>
    <div>
        <div id="supplier-photo" style="float:left;">
            {{ HTML::image(Auth::supplier()->get()->photo_name, 'logo_supplier', array('class' => 'img-thumbnail',
            'width' => '350em', 'id' => 'profile-photo' )) }}
        </div>
        <div id="supplier-description">
            <p class="text-info">{{ Auth::supplier()->get()->description }}</p>
        </div>
    </div>

    <!-- TODO: Esto será sustituído por los clientes potenciales -->
    <div class="clearfix"></div>
    <div>
        <h1>{{trans('pattern.lista')}}
            <small>{{Lang::get('pattern.estudiantes')}}</small>
        </h1>
        <div id="list-students">
            {{ Datatable::table()
            ->addColumn(trans('pattern.nombre'), trans('pattern.apellidos'), trans('pattern.carrera'), trans('pattern.correo')) // these are the column headings to be shown
            ->setUrl(route('api.supplier.showProfile')) // this is the route where data will be retrieved
            ->setOptions('responsive', 'true')
            ->render() }}
        </div>
    </div>
</div>
@stop
{{ HTML::script('assets/js/jquery-2.1.3.min.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/jquery.dataTables.min.js' ) }}
{{ HTML::script('assets/js/dataTables.responsive.min.js' ) }}
{{ HTML::script('assets/js/utils.js') }}
{{ HTML::script('//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json') }}