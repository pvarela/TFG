<?php

Class SuppliersController extends BaseController
{

    public function storeSupplier()
    {
        $response = Supplier::validateSupplier(Input::all());
        if ($response['error'] == true) {
            return Redirect::to('supplier')->withErrors($response['mensaje'])->withInput();
        } else {
            return Redirect::to('supplier/showProfile')->with('mensaje', $response['mensaje']);
        }
    }

    public function newSupplier()
    {
        $labels = $this->getLabels();
        return View::make('new-supplier')->with(['labels' => $labels]);
    }

    public function activeSupplier($activation)
    {
        $user_to_activate = Supplier::where('activation', '=', $activation)->where('active', '=', 0)->first();
        if ($user_to_activate) {
            Supplier::where('activation', '=', $activation)->update(array('active' => 1));
            $this->loginSupplier();
        } else {
            //TODO: Hacer algo para mostrar el mensaje de error
            Session::flash("Error activación", "No se puede activar la cuenta. Contacte con el administrador");
        }
    }

    public function autoActiveSupplier($uid, $type)
    {

        $user_to_activate = null;
        if ($type === 'google') {
            $user_to_activate = Supplier::where('uid_google', '=', $uid)->first();
            if ($user_to_activate) {
                Supplier::where('uid_google', '=', $uid)->update(array('active' => 1));
            }

        } else if ($type === 'facebook') {
            $user_to_activate = Supplier::where('uid_fb', '=', $uid)->first();
            if ($user_to_activate) {
                Log::info("activo fb");
                Supplier::where('uid_fb', '=', $uid)->update(array('active' => 1));

            }
        }

        Auth::supplier()->login($user_to_activate);

    }

    public function showProfile()
    {
        if (Auth::supplier()->check()) {
            return View::make('administration.supplier-administration');
        }

        return Redirect::to('/');
    }

    public function editProfile()
    {
        $labelsSelect = array();
        $labels = array();

        if (Auth::supplier()->check()) {

            $supplier = Auth::supplier()->get();
            $labelsSelect = $this->getLabels();

//            Log::info(print_r($labelsSelect, true));
            return View::make('new-supplier')->with(['supplier' => $supplier, 'labels' => $labelsSelect]);
        }

        return Redirect::to('/');
    }

    public function getDatatable()
    {
        if (Auth::supplier()->check()) {
            return Datatable::collection(Auth::supplier()->get()->students->unique())
                ->showColumns('name', 'surname', 'career', 'email')
                ->searchColumns('name', 'surname', 'career', 'email')
                ->orderColumns('name', 'surname', 'career', 'email')
                ->make();
        }
    }

    public function getGeocoding($address)
    {

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . Config::get('googleCoding.api_key_server');
        Log::info($url);
        $json = json_decode(file_get_contents($url), true);
        Log::info(print_r($json, true));
        $long = $json['results'][0]['geometry']['location']['lng'];
        $lat = $json['results'][0]['geometry']['location']['lat'];
        return array($lat, $long);
    }

    public function showPageSupplier($id)
    {
        $supplier = Supplier::where('id', '=', $id)->first();
        $city = $supplier->city;
        $coordenades = $this->getGeocoding($city);
        $comments = Comment::orderBy('created_at', 'desc')->where('service_supplier_id', '=', $id)->paginate(5);
        $labels = array();
        $labelsName = array("all" => Lang::get("perfil.all"));

        foreach($comments as $comment){
            $aux = Comment::getLabels($comment);
            foreach($aux as $key=>$value){
                array_push($labels, $value);

            }
        }

        $labels = array_unique($labels);
        foreach ($labels as $label){
            Log::info($label);
            if($label != ""){
                $labelsName[(string)($label)] = $label;
            }
        }

        $services = $supplier->services()->get();

        return View::make('supplier')->with(['supplier' => $supplier, 'coordenades' => $coordenades, 'comments' => $comments, 'labels' => $labelsName, 'services' => $services]);
    }

    public function getStudentInformation()
    {
        $student = Auth::student()->get();
        $data = Input::all();
        $supplier = Supplier::find($data['supplierID']);
        $comments = Comment::orderBy('created_at', 'desc')->where('service_supplier_id', '=', $data['supplierID'])->get();
        $student->suppliers()->attach($data['supplierID']);

        $city = $supplier->city;
        $coordenades = $this->getGeocoding($city);

        $dataMessage = array(
            'name' => $data['name'],
            'surname' => $data['surname'],
            'telephone' => $data['phone'],
            'birthday' => $student->birthday,
            'career' => $student->career,
            'university' => $student->university,
            'email' => $student->email,
            'photo_name' => $student->photo_name,
            'hourCall' => $data['hourCall'],
            'dateCall' => $data['dateCall']);

        Mail::send('emails.info-student', $dataMessage, function ($message) {
            $message->to(Input::get('emailSupplier'), Input::get('name'))->subject('Solicitud de información de un estudiante');
        });

        $message = "Su información ha sido enviada a la empresa. Se pondrán en contacto con usted";

        return Redirect::route('supplier.id', array('id' => $data['supplierID']))->with(['message' => $message, 'supplier' => $supplier, 'comments' => $comments, 'coordenades' => $coordenades]);
    }

    public function getLabels()
    {
        $labelsSelect = array();
        $labels = DB::table('labels')->whereNotExists(function($query)
        {
            $query->select(DB::raw('*'))
                ->from('comments_has_labels')
                ->whereRaw('labels.id = comments_has_labels.labels_id');
        })->get();

        foreach ($labels as $label) {
            if(sizeof($label) > 0){
                $labelsSelect[$label->name] = $label->name;
            }
        }

        return $labelsSelect;
    }
}