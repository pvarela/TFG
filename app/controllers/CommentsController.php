<?php

/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 15/07/2015
 * Time: 20:35
 */
class CommentsController extends BaseController
{

    public function storeComment($id){

        Log::info(Input::all());
        $response = Comment::validateComment(Input::all());

        if ($response['error'] == true) {
            return Redirect::back()->withErrors($response['mensaje'])->withInput();
        } else {
            return Redirect::back()->with('mensaje', $response['mensaje']);
        }
    }

    public static function getComments(){
        $comments = Comment::orderBy('created_at', 'desc')->all();
        return $comments;
    }

    public function ajaxSearchComment(){

        $data = Input::all();
        $sex = $data['option-sex'];
        $reputation = $data['reputation'];
        $supplier = $data['supplier'];
        $students = null;

        if($sex != "all"){
            $students = Student::where('sex', '=', $sex)->get(['id']);
        }else{
            $students = Student::where('sex', '=', 'male')->orWhere('sex', '=', 'female')->get(['id']);
        }

        $min = explode(" - ", $reputation)[0];
        $max = explode(" - ", $reputation)[1];

        if (sizeof($students) > 0){
            $query = "service_supplier_id=" . $supplier . " and mark>=" . $min . " and mark<=" . $max . " and (";
        }else{
            $query = "0=1";
        }

        for ($i=0; $i<sizeof($students); $i++){
            if ($i != sizeof($students) -1){
                $query = $query . "Estudiantes_id=" . $students[$i]['id'] . " or ";
            }else {
                $query = $query . "Estudiantes_id=" . $students[$i]['id'] .")";
            }
        }

        $comments = Comment::whereRaw($query)->get();
        $supplier = Supplier::where('id', '=', $supplier)->first();
        $city = $supplier->city;
        $coordenades = $this->getGeocoding($city);
        return View::make('ajax.ajaxComments')->with(['supplier' => $supplier, 'coordenades' => $coordenades, 'comments' => $comments]);
    }

    public function getGeocoding($address)
    {

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . Config::get('googleCoding.api_key_server');
        $json = json_decode(file_get_contents($url), true);
        $long = $json['results'][0]['geometry']['location']['lng'];
        $lat = $json['results'][0]['geometry']['location']['lat'];
        return array($lat, $long);
    }

}