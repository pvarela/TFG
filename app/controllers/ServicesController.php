<?php

Class ServicesController extends BaseController
{

    public function storeService()
    {
        $response = Service::validateService(Input::all());

        if ($response['error'] == true) {
            return Redirect::to('services/new')->withErrors($response['mensaje'])->withInput();
        } else {
            return Redirect::to('services/new')->with('mensaje', $response['mensaje']);
        }
    }

    public function getLabels()
    {
        $arrayLabels = array();
        $arrayIds = array();
        $labels = DB::table('labels')->whereNotExists(function($query)
        {
            $query->select(DB::raw('*'))
                ->from('comments_has_labels')
                ->whereRaw('labels.id = comments_has_labels.labels_id');
        })->get();

        foreach($labels as $label){
            $value = $label->id;
            $name = $label->name;
            $arrayLabels[$value] = $name;
            array_push($arrayIds, $value);
        }

        return View::make('administration.add-services')->with(['labels' => $arrayLabels, 'ids' => $arrayIds]);
    }

    public function editService()
    {
        $servicesArray = array('0' => Lang::get('service.select'));
        $supplier = Auth::supplier()->get();

        $services = $supplier->services()->get();

        foreach($services as $service){
            $servicesArray[$service->id] = $service->name;
        }

        return View::make('administration.edit-services')->with(array('servicesSelect' => $servicesArray));
    }

    public function getFormEditService()
    {
        $arrayLabels = array();

        $labels = DB::table('labels')->whereNotExists(function ($query) {
            $query->select(DB::raw('*'))
                ->from('comments_has_labels')
                ->whereRaw('labels.id = comments_has_labels.labels_id');
        })->get();

        foreach ($labels as $label) {
            $value = $label->id;
            $name = $label->name;
            $arrayLabels[$value] = $name;
        }

        if (Request::ajax()){
            $serviceID = Input::get('service');
            $service = Service::where('id', '=', $serviceID)->first();
            $labelsSelected = Service::getLabelsOfAService($service);
            $selected = array();
            foreach ($labelsSelected as $labelsSelected) {
                array_push($selected, $labelsSelected->labels_id);
            }
            return View::make('ajax.ajaxService')->with(['service' => $service, 'labels' => $arrayLabels, 'labelsSelected' => $selected]);
        }else{
            $data = Input::all();
            $response = Service::validateEditService($data);

            if ($response['error'] == true) {
                return Redirect::to('services/edit')->withErrors($response['mensaje'])->withInput()->with('mensaje-error', $response['mensaje']);
            } else {
                return Redirect::to('supplier/showProfile')->with('mensaje', $response['mensaje']);
            }
        }
    }

    public function showDeleteService(){
        $supplier = Auth::supplier()->get();
        $services = $supplier->services()->get();
        return View::make('administration.delete-services')->with('services', $services);
    }

    public function deleteService(){
        $data = Input::all();
        $response = Service::deleteServices($data);

        if($response['error'] == true) {
            return Redirect::to('services/delete')->withErrors($response['mensaje'])->withInput();
        }else{
            return Redirect::to('services/delete')->with('mensaje', $response['mensaje']);
        }
    }
}