<?php

class LessonsController extends BaseController
{
    public function storeLesson()
    {
        $response = Lesson::validateLesson(Input::all());

        if ($response['error'] == true) {
            return Redirect::to('lessons/new')->withErrors($response['mensaje'])->withInput();
        } else {
            return Redirect::to('lessons/new')->with('mensaje', $response['mensaje']);
        }
    }

    public function getServices()
    {
        $id = Auth::supplier()->get()->id;
        $services = Supplier::find($id)->services()->get();
        $arrayServices = array();
        foreach($services as $service){
            $value = $service['id'];
            $name = $service['name'];
            $arrayServices[$value] = $name;
        }
        return View::make('administration.add-lessons')->with('services', $arrayServices);
    }

    public function editLesson()
    {
        $lessonsArray = array('0' => Lang::get('lesson.select'));
        $supplier = Auth::supplier()->get();

        $lessons = $supplier->lessons()->get();

        foreach($lessons as $lesson){
            $lessonsArray[$lesson->id] = $lesson->subject . " " . $lesson->level;
        }

        return View::make('administration.edit-lessons')->with(array('lessonsSelected' => $lessonsArray));
    }

    public function getFormEditLesson()
    {

        if (Request::ajax()){
            $lessonID = Input::get('lesson');
            $lesson = Lesson::where('id', '=', $lessonID)->first();
            $tariff = Lesson::getTariffOfALesson($lesson);
            $timetables = Lesson::getTimetablesOfALesson($lesson);
            $count = sizeof($timetables);

            if (sizeof($timetables) > 0) {
                $timetables = $timetables[0];
            }else{
                $timetables = null;
            }
            return View::make('ajax.ajaxLesson')->with(['lesson' => $lesson, 'tariff' => $tariff, 'timetables' => $timetables, 'count' => $count]);
        }else{
            $data = Input::all();
            $response = Lesson::validateEditLesson($data);

            if ($response['error'] == true) {
                return Redirect::to('lessons/edit')->withErrors($response['mensaje'])->withInput()->with('mensaje-error', $response['mensaje']);
            } else {
                return Redirect::to('supplier/showProfile')->with('mensaje', $response['mensaje']);
            }
        }
    }

    public function showDeleteLesson()
    {
        $supplier = Auth::supplier()->get();
        $lessons = $supplier->lessons()->get();
        return View::make('administration.delete-lesson')->with('lessons', $lessons);
    }

    public function deleteLesson()
    {
        $data = Input::all();
        $response = Lesson::deleteLessons($data);

        if($response['error'] == true) {
            return Redirect::to('lessons/delete')->withErrors($response['mensaje'])->withInput();
        }else{
            return Redirect::to('lessons/delete')->with('mensaje', $response['mensaje']);
        }
    }
}
