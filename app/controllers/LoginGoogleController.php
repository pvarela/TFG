<?php

class LoginGoogleController extends \BaseController {

    public function loginWithGoogle() {

        // get data from input
        $code = Input::get('code');
        $student = null;
        // get google service
        $googleService = OAuth::consumer( 'Google', 'http://localhost/tfg/public/login/google' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken( $code );

            // Send a request with it

            $result = json_decode( $googleService->request( 'https://www.googleapis.com/plus/v1/people/me' ), true );
            Log::info($result);

            if (Student::where('uid_google', '=', $result['id'])->exists() && Session::get('login') === 'login'){
                Session::put('name', 'user');
            } else if (Supplier::where('uid_google', '=', $result['id'])->exists() && Session::get('login') === 'login'){
                Session::put('name', 'supplier');
            }

            if (Session::get('name') === 'user' && Student::where('uid_google', '=', $result['id'])->first()){

                $student = Student::where('uid_google', '=', $result['id'])->first();
                $studentID = $student->id;
                $student = Student::find($studentID);
                Auth::student()->login($student);
                return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Google +');

            }else if(Session::get('name') === 'supplier' && Supplier::where('uid_fb', '=', $result['id'])->first()){

                $supplier = Supplier::where('uid_google', '=', $result['id'])->first();
                $supplierID = $supplier->id;
                $supplier = Supplier::find($supplierID);
                Auth::supplier()->login($supplier);
                return Redirect::to('/supplier/showProfile')->with('message', 'Inicio de sesión correcto usando Google +');

            }else{

                if (Session::get('name') === 'user'){
                    $student = Student::where('uid_google', '=', $result['id'])->first();

                    if(empty($student)){

                        $student = new Student();
                        $student->name = $result['name']['givenName'];
                        $student->surname = $result['name']['familyName'];
                        $student->email = last($result['emails'])['value'];
                        $student->photo_name = $result['image']['url'];
                        $student->username = Student::generateUserName($student->name, $student->surname);
                        $student->sex = $result['gender'];
                        $student->university = last($result['organizations'])['name']?last($result['organizations'])['name']:"";
                        $student->career = last($result['organizations'])['title']?last($result['organizations'])['title']:"";
                        $student->description = $result['tagline']?$result['tagline']:"";
                        $student->uid_google = $result['id'];
                        $student->save();
                    }

                    $student->access_token_google = $token->getAccessToken();
                    $student->save();

                    $usersController = App::make('UsersController');
                    $usersController->callAction('autoActiveUser', array($student->uid_google, 'google'));

                    if(Auth::student()->check() != 1){
                        Auth::login($student);
                        Log::info(Auth::student()->check());

                    }

                    return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Google +');



                }else{

                    $supplier = Supplier::where('uid_google', '=', $result['id'])->first();

                    if(empty($supplier)){

                        $supplier = new Supplier();
                        $supplier->name =  $result['name']['givenName'];
                        $supplier->email = last($result['emails'])['value'];
                        $supplier->photo_name = $result['image']['url'];
                        $supplier->reputation = 5;
                        $supplier->uid_google = $result['id'];
                        $supplier->save();
                    }

                    $supplier->access_token_google = $token->getAccessToken();
                    $supplier->save();

                    $suppliersController = App::make('SuppliersController');
                    $suppliersController->callAction('autoActiveSupplier', array($supplier->uid_google, 'google'));

                    if(Auth::supplier()->check() != 1){
                        Auth::login($supplier);
                        Log::info(Auth::supplier()->check());
                    }

                    return Redirect::to('/supplier/showProfile')->with('message', 'Inicio de sesión correcto usando Google +');

                }
            }
        }
        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();
            // return to google login url//
            return Redirect::to( (string)$url );
        }
    }
}

