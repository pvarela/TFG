<?php

class LoginFacebookController extends \BaseController {

	private $fb;
    private $kindSignUp;

    public function __construct(FacebookHelper $fb){
        $this->fb = $fb;
    }

    public function login(){
        return Redirect::to($this->fb->getUrlLogin());
    }

    public function callback(){

        if( !$this->fb->generateSessionFromRedirect() ){
            return Redirect::to('/')->with("message", "Error de conexión con Facebook");
        }
        $user_fb = $this->fb->getGraph();

        if(empty($user_fb)){
            return Redirect::to('/')->with("message", "Error al obtener el usuario de Facebook");
        }
        // Antes debemos saber si ya se encuentra registrado el usuario con ese $uid_fb y en qué tabla
        $usuarioFacebook = $user_fb->getProperty('id');
        if ( Student::where('uid_fb', $usuarioFacebook)->first() && Session::get('login') === 'login'){

            $student = Student::where('uid_fb', $usuarioFacebook)->first();
            $studentID = $student->id;
            $student = Student::find($studentID);
            Auth::student()->login($student);
            return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');
        }

        else if ( Supplier::where('uid_fb', $usuarioFacebook)->exists() && Session::get('login') === "login") {
            $supplier = Supplier::where('uid_fb', $usuarioFacebook)->first();
            $supplierID = $supplier->id;
            $supplier = Supplier::find($supplierID);
            Auth::supplier()->login($supplier);
            return Redirect::to('/supplier/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');
        }

        Log::info(Session::get('name'));
        if (Session::get('name') === 'user' && Student::where('uid_fb', $usuarioFacebook)->first()){
            $student = Student::where('uid_fb', $usuarioFacebook)->first();
            $studentID = $student->id;
            $student = Student::find($studentID);
            Auth::student()->login($student);
            return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');

        }

        else if (Session::get('name') === 'supplier' && Supplier::where('uid_fb', $usuarioFacebook)->first()){

            $supplier = Supplier::where('uid_fb', $usuarioFacebook)->first();
            $supplierID = $supplier->id;
            $supplier = Supplier::find($supplierID);
            Auth::supplier()->login($supplier);
            Log::info("deberia irse aqui");
            return Redirect::to('/supplier/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');


        }else{
            // Registro de usuarios
            Log::info("hola ser");
            $this->kindSignUp = Session::get('name');
            Log::info($this->kindSignUp);

            if ($this->kindSignUp === 'supplier') {

                Log::info("tengo que registrar al servicio");
                $supplier = Supplier::whereUidFb($user_fb->getProperty('id'))->first();

                if(empty($supplier)){

                    $supplier = new Supplier();
                    $supplier->name = $user_fb->getProperty('first_name');
                    $supplier->email = $user_fb->getProperty('email');
                    $supplier->photo_name = 'http://graph.facebook.com/' . $user_fb->getProperty('id') . '/picture?type=large';
                    $supplier->reputation = 5;
                    $supplier->uid_fb = $user_fb->getProperty('id');

                    $supplier->save();
                }

                $supplier->access_token_fb = $this->fb->getToken();
                $supplier->save();

                $suppliersController = App::make('SuppliersController');
                $suppliersController->callAction('autoActiveSupplier', array($supplier->uid_fb, 'facebook'));

                if(Auth::supplier()->check() != 1){
                    Auth::login($supplier);
                    Log::info(Auth::supplier()->check());

                }

                return Redirect::to('/supplier/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');

            }
            else{

                $user = Student::whereUidFb($user_fb->getProperty('id'))->first();

                if(empty($user)){
                    $user = new Student();
                    $user->name = $user_fb->getProperty('first_name');
                    $user->surname = $user_fb->getProperty('last_name');
                    $user->email = $user_fb->getProperty('email');
                    $user->birthday = date("Y-m-d", strtotime($user_fb->getProperty('birthday')));
                    $user->photo_name = 'http://graph.facebook.com/' . $user_fb->getProperty('id') . '/picture?type=large';
                    $user->username = Student::generateUserName($user->name, $user->surname);
                    $user->sex = $user_fb->getProperty('gender');
//                $education = $user_fb->getProperty('education');
                    $user->uid_fb = $user_fb->getProperty('id');
                    $user->save();
                }

                $user->access_token_fb = $this->fb->getToken();
                $user->save();
                $usersController = App::make('UsersController');
                $usersController->callAction('autoActiveUser', array($user->uid_fb, 'facebook'));

                if(Auth::student()->check() != 1){
                    Auth::login($user);
                    Log::info(Auth::student()->check());

                }

                return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');

            }
        }

//        return Redirect::to('/student/showProfile')->with('message', 'Inicio de sesión correcto usando Facebook');
    }

    public function getSchoolYear()
    {

    }
}
