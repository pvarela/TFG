<?php

Class UsersController extends BaseController
{

    public function storeUser()
    {
        $response = Student::validateUser(Input::all());

        if ($response['error'] == true) {
            return Redirect::to('user')->withErrors($response['mensaje'])->withInput();
        } else {
            return Redirect::to('user')->with('mensaje', $response['mensaje']);
        }
    }

    public function newUser()
    {
        return View::make('new-user');
    }

    public function activeUser($activation)
    {
        $user_to_activate = Student::where('activation', '=', $activation)->where('active', '=', 0)->first();
        if ($user_to_activate) {
            Student::where('activation', '=', $activation)->update(array('active' => 1));
            Auth::student()->loginUsingId($user_to_activate->id);
            return Redirect::to('student/showProfile');
        } else {
            Session::flash("Error activación", "No se puede activar la cuenta. Contacte con el administrador");
        }
    }

    public function autoActiveUser($uid, $type)
    {
        $user_to_activate = null;
        if($type === 'google'){
            $user_to_activate = Student::where('uid_google', '=', $uid)->first();
            if ($user_to_activate) {
                Log::info("activo");
                Student::where('uid_google', '=', $uid)->update(array('active' => 1));
            }

        }else if ($type === 'facebook'){
            $user_to_activate = Student::where('uid_fb', '=', $uid)->first();
            if ($user_to_activate) {
                Student::where('uid_fb', '=', $uid)->update(array('active' => 1));

            }
        }

        Auth::student()->login($user_to_activate);

    }

    public function showProfile()
    {
        if (Auth::student()->check()) {
            $student = Auth::student()->get();
            $comments = $student->comments()->get();
            $suppliers = $student->suppliers()->get();
            $services = array();

            foreach($suppliers as $supplier){
                array_push($services, $supplier->services()->get());
            }

            $services = array_unique($services);
            Log::info($student->id);
            Log::info(print_r($comments, true));
            print_r($comments, true);
            return View::make('administration.user-administration')->with(array('comments' => $comments, 'services' => $services));
        }

        return Redirect::to('/');
    }

    public function editProfile()
    {
        if (Auth::student()->check()) {
            $student = Auth::student()->get();
            return View::make('new-user')->with(['student' => $student]);
        }
    }

    public function applyInfo()
    {
        $student = Auth::student()->get();
        $name = $student->getName();
        $surname =$student->getSurname();
        $telephone = $student->getTelephone();
        $data = Input::all();
        $dateCall = $data['date-call'];
        $hourCall = $data['hour-call'];
        $emailSupplier = $data['emailSupplier'];
        $supplierID = $data['supplierID'];
        $supplier = Supplier::find($supplierID);
        return View::make('apply-info', compact('name', 'surname', 'telephone', 'dateCall', 'hourCall', 'emailSupplier', 'supplierID', 'supplier'));
    }
}