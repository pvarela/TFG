<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('/layouts/layout');
	}

    public function getIndexPage()
    {
        $services = Service::all();
        return View::make('index')->with('services', $services);
    }


    public function loginFacebook()
    {
        Session::put('login', 'login');
        return Redirect::to('login/fb');
    }


    public function loginGoogle()
    {
        Session::put('login', 'login');
        return Redirect::to('login/google');
    }


    public function signUp()
    {
        //check which submit was clicked on
        if(Input::get('signup-button-submit')) {
            return $this->normalSignUp(); //if login then use this method
        } else if(Input::get('method') === 'facebook'){
            Log::info("debo entrar en facebook");
            return $this->signUpFacebook(); //if register then use this method
        } else if(Input::get('method') === 'google'){
            return $this->signUpGoogle();
        }

    }

    public function normalSignUp()
    {
        $data = Input::all();
        Log::info("con esta data");
        Log::info($data);
        $kindSignUp = $data['name'];

        if ($kindSignUp === 'user') {
            return Redirect::to('/user');
        } else {
            return Redirect::to('/supplier');
        }
    }

    public function signUpFacebook()
    {
        Log::info("en facebook");
        Log::info(Input::all());
        $data = Input::all();
        $kindSignUp = $data['name'];
        Session::put('name', $kindSignUp);
        Session::put('login', 'signup');
        return Redirect::to('/login/fb');

    }

    public function signUpGoogle(){
        $data = Input::all();
        $kindSignUp = $data['name'];
        Session::put('name', $kindSignUp);
        Session::put('login', 'signup');
        return Redirect::to('/login/google');
    }

    public function searchServices()
    {
        $keyword = Input::get('keyword');
        $services = Service::where('name', 'LIKE', '%'.$keyword.'%')->get();
        $cities = DB::table('services')->select('city')->distinct()->get();
        $cities = json_decode(json_encode($cities), true);

        $arrayLabels = array();
        $arrayIds = array();
        $labels = DB::table('labels')->whereNotExists(function($query)
        {
            $query->select(DB::raw('*'))
                ->from('comments_has_labels')
                ->whereRaw('labels.id = comments_has_labels.labels_id');
        })->get();

        foreach($labels as $label){
            $value = $label->id;
            $name = $label->name;
            $arrayLabels[$value] = $name;
            array_push($arrayIds, $value);
        }

        return View::make('results')->with(['services' => $services, 'cities' => $cities, 'labels' => $arrayLabels]);
    }

    public function ajaxSearchServices()
    {
        $data = Input::all();
        $keysData = array();
        $keys = array("city", "reputation", "label");
        $queryCity = "";
        $queryReputation = "";
        $queryLabel = "";
        $query = "";

        foreach($data as $key => $value){
            array_push($keysData, $key);
        }

        if(in_array("city", $keysData)){
            $cities = $data['city'];
            for($i=0; $i<count($cities); ++$i){
                if($i==count($cities)-1){
                    $queryCity = $queryCity . "city='" . $cities[$i] . "'";
                }else{
                    $queryCity = $queryCity . " city='" . $cities[$i] . "' or ";
                }
            }
        }

        $reputation = explode(" - ", $data['reputation']);
        $min = $reputation[0];
        $max = $reputation[1];
        $queryReputation = "reputation>=" . $min . " and reputation<=" .$max;

        if(in_array("label", $keysData)){
            $labels = $data['label'];
            $servicesObjects = array();
            $servicesIDs = array();

            foreach($labels as $label){
                $servicesObjects = DB::table('services_has_labels')->where('labels_id', '=', $label)->get();
                foreach($servicesObjects as $service){
                    array_push($servicesIDs, $service->services_id);
                }
            }

            for($i=0; $i<count($servicesIDs); ++$i){
                if($i==count($servicesIDs)-1){
                    $queryLabel = $queryLabel . "id=" . (string)$servicesIDs[$i];
                }else{
                    $queryLabel = $queryLabel . " id=" . (string)$servicesIDs[$i] . " or ";
                }
            }
        }

//        Log::info($queryCity);
//        Log::info($queryReputation);
//        Log::info($queryLabel);
        $query = $queryReputation;

        if ($queryCity != "" and $queryReputation != "" ){
            $query = "(" . $queryCity . ") and " . "(" . $queryReputation . ")";
        }

        if ($queryLabel != "" ){
            $query = "(" . $query . ") and " . "(" . $queryLabel . ")";
        }



        Log::info($query);
        $services = Service::whereRaw($query)->get();
        return View::make('ajax.ajaxResults')->with('services', $services);
//        return Response::json($services);
    }

    /* Barra superior */

    public function getAcademiesSubject()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Academias')->orWhere('name', '=', 'Academies')->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('academies')->with(['services' => $services, 'kind' => 'academies']);
    }

    public function getLanguageAcademies()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Academias de Idiomas')->orWhere('name', '=', 'Language academies')->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('academies')->with(['services' => $services, 'kind' => 'academies']);
    }

    public function getOfficialLanguageSchools()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Escuelas Oficiales de Idiomas')->orWhere('name', '=', 'Official Language Schools')->distinct()->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('academies')->with(['services' => $services, 'kind' => 'academies']);
    }

    public function getDorms()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Colegios Mayores')->orWhere('name', '=', 'Residencias')->orWhere('name', '=', 'Dorms')->orWhere('name', '=', 'Colleges')->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('accommodations')->with(['services' => $services, 'kind' => 'accommodations']);
    }

    public function getFlats()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Escuelas Oficiales de Idiomas')->orWhere('name', '=', 'Official Language Schools')->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('accommodations')->with(['services' => $services, 'kind' => 'accommodations']);
    }

    public function getHousings()
    {
        $labelsAcademie = DB::table('labels')->where('name', '=', 'Escuelas Oficiales de Idiomas')->orWhere('name', '=', 'Official Language Schools')->get();
        $services = array();
        foreach ($labelsAcademie as $label) {
            $servicesLabel = Label::getServicesForALabel($label);
            foreach($servicesLabel as $service){
                array_push($services, $service);
            }
        }
        $services = array_unique($services);
        return View::make('accommodations')->with(['services' => $services, 'kind' => 'accommodations']);
    }

    public function getAcademie($id){
        $service = Service::where('id', '=', $id)->first();
        $supplier = $service->supplier()->first();
        $comments = Comment::orderBy('created_at', 'desc')->where('services_id', '=', $id)->paginate(5);
        $labels = array();
        $labelsName = array("all" => Lang::get("perfil.all"));

        foreach($comments as $comment){
            $aux = Comment::getLabels($comment);
            foreach($aux as $key=>$value){
                array_push($labels, $value);

            }
        }

        $labels = array_unique($labels);
        foreach ($labels as $label){
            Log::info($label);
            if($label != ""){
                $labelsName[(string)($label)] = $label;
            }
        }
        $lessons = Service::getLessonsOfAService($service);
        return View::make('academie')->with(array('service' => $service, 'lessons' => $lessons, 'comments' => $comments, 'labels' => $labelsName, 'supplier' => $supplier));
    }

    public function getAccommodation($id){
        $service = Service::where('id', '=', $id)->first();
        $supplier = $service->supplier()->first();
        $comments = Comment::orderBy('created_at', 'desc')->where('services_id', '=', $id)->paginate(5);
        $labels = array();
        $labelsName = array("all" => Lang::get("perfil.all"));

        foreach($comments as $comment){
            $aux = Comment::getLabels($comment);
            foreach($aux as $key=>$value){
                array_push($labels, $value);

            }
        }

        $labels = array_unique($labels);
        foreach ($labels as $label){
            Log::info($label);
            if($label != ""){
                $labelsName[(string)($label)] = $label;
            }
        }

        return View::make('accommodation')->with(['service' => $service, 'comments' => $comments, 'labels' => $labelsName,'supplier' => $supplier]);
    }
}
