<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 * Date: 6/06/15
 * Time: 12:50
 */

return array(
    'academias' => 'Academies',
    'alojamientos' => 'Accommodation',
    'info' => 'Useful information',
    'ocio' => 'Leisure',
    'web' => 'Student official web',
    'login' => 'Log in',
    'signup' => 'Sign up',
    'perfil' => 'Profile',
    'editarPerfil' => 'Edit profile',
    'logout' => 'Log out',
    'recordar' => 'Remember password?',
    'olvidada' => 'Forgot your password?',
    'buscar' => 'Search',
    'becas' => 'Scholarships',
    'universidades' => 'Public/Private Universities',
    'tarjetas' => 'Student card',
    'tandem' => 'Tandem',
    'asignaturas' => 'Academies',
    'idiomas' => 'Language Academies',
    'escuelas' => 'Official Language School',
    'colegios' => 'Dorms',
    'pisos' => 'Flats for rent',
    'residencias' => 'Student housing',
    'agencias' => 'Travel agencies',
    'gimnasios' => 'Gyms',
    'tandem' => 'Tandems',
    'servicios' => 'Services',
    'clases' => 'Lessons',
    'administracion' => 'Administration',
    'estudiantes' => 'Students',
    'lista' => 'List',
    'nombre' => 'Name',
    'apellidos' => 'Surname',
    'carrera' => 'Career',
    'correo' => 'E-mail',
    'facebook' => 'Log in with Facebook',
    'iamstudent' => "I'm student",
    'iamcompany' => "I'm a company",
    'loginWith' => 'Login with',
    'signupWith' => 'Sign up with',
    'siguenos' => 'Follow us',
    'añadir_servicios' => 'Add service',
    'editar_servicios' => 'Edit service',
    'eliminar_servicios' => 'Remove services',
    'añadir_cursos' => 'Add lesson',
    'editar_cursos' => 'Edit lesson',
    'eliminar_cursos' => 'Remove services',



);
