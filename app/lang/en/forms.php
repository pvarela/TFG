<?php
return array(
    'nombre' => 'Name',
    'apellidos' => 'Surname',
    'correo' => 'Email',
    'foto' => 'Picture',
    'fecha-nacimiento' => 'Birthday',
    'sexo' => 'Sex',
    'hombre' => 'Male',
    'mujer' => 'Female',
    'universidad' =>'University',
    'estudios' => 'Career',
    'curso' => 'School year',
    'descripcion' => 'Description',
    'registrar' => 'Sign up',
    'nombre-empresa' => 'Company name',
    'contraseña' => 'Password',
    'confirma' => 'Confirm',
    'proveedor-servicios' => 'Companies'
);