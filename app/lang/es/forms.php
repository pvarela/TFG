<?php
return array(
    'nombre' => 'Nombre',
    'apellidos' => 'Apellidos',
    'correo' => 'Correo electrónico',
    'foto' => 'Foto',
    'fecha-nacimiento' => 'Fecha de nacimiento',
    'sexo' => 'Sexo',
    'hombre' => 'Hombre',
    'mujer' => 'Mujer',
    'universidad' =>'Universidad',
    'estudios' => 'Estudios',
    'curso' => 'Curso',
    'descripcion' => 'Descripción',
    'registrar' => 'Registrar',
    'nombre-empresa' => 'Nombre de la empresa',
    'contraseña' => 'Contraseña',
    'confirma' => 'Confirma',
    'proveedor-servicios' => 'Empresas'

);