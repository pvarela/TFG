<?php

class MockFacadeTest extends TestCase {

    public function setUp()
    {
        parent::setUp();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    }

    public function endTests()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }



    public function testFailGetSupplier()
    {
        $this->call('GET', 'supplier/showProfile');
        $this->assertResponseStatus(302);
    }


    public function testGetSupplier()
    {
        $supplier = Supplier::first();
        Auth::supplier()->loginUsingId($supplier->id);
        $this->call('GET', 'supplier/showProfile');
        $this->assertResponseOk();
    }

    public function testNewService()
    {
        $supplier = Supplier::first();
        Auth::supplier()->loginUsingId($supplier->id);

        $service = array(
            'name' => 'Academia Test',
            'email' => 'test@universityLife.com',
            'web' => 'http://www.universityLife.com',
            'telephone' => '654342512',
            'description' => 'Un servicio de prueba',
            'city' => 'Madrid',
            'street' => 'Mesón de Paredes',
            'number' => '76',
            'labels' => array('1' => 'Academias')
        );

        $this->call('POST', 'supplier/services/new', $service, ['photo_name' => new Symfony\Component\HttpFoundation\File\UploadedFile(
            'C:/xampp/htdocs/tfg/public/img/users/pedvarllo.2015-02-07-06-02-16/pedvarllo.2015-02-07-06-02-16.jpeg', 'pedvarllo.2015-02-07-06-02-16.jpeg')]);
        $this->assertResponseStatus(302);
    }

    public function testDeleteServices()
    {
        $supplier = Supplier::first();
        Auth::supplier()->loginUsingId($supplier->id);
        $servicesTest = Service::where('name', '=', 'Academia Test')->get();
        $this->call('POST', 'services/delete', array($servicesTest));
    }
}