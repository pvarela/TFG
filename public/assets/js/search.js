$(function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 5,
        values: [ 0, 5 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 ) +
        " - " + $( "#slider-range" ).slider( "values", 1 ) );

    var startPos = '$("#slider").slider("value");';
    var endPos = '';

    $("#slider-range").on("slidestop", function(event, ui) {
        endPos = ui.value;

        if (startPos != endPos) {
            var pathname = window.location.pathname.split("/");
            var supplier = pathname[pathname.length-1];
            ajaxSearch(supplier);
        }

        startPos = endPos;
    });
});

$(".checkbox").change(function() {
    var pathname = window.location.pathname.split("/");
    var supplier = pathname[pathname.length-1];
    ajaxSearch();
});

function ajaxSearch(){
    var $inputs = $('#search-form :input');
    var values = {};
    $inputs.each(function(index) {
        if ((this).type == "checkbox"){
            if ($(this).is(":checked")){
                values[this.name + index.toString()] = ($(this).val());
            }
        }else if(this.type == "text"){
            values[this.name] = $(this).val();
        }
    });

    $.ajax({
        url: 'ajaxSearch',
        type: 'POST',
        data: values,
        success: function(response){
            console.log(response);
            $("#results").html(response);
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.error(textStatus);
        }
    });
}