$(document).ready(function(){

    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").alert('close');
    });

    /* Multiple select choice without ctrl click */

    $('.select-toggle').each(function(){
        var select = $(this), values = {};
        $('option',select).each(function(i, option){
            values[option.value] = option.selected;
        }).click(function(event){
            values[this.value] = !values[this.value];
            $('option',select).each(function(i, option){
                option.selected = values[option.value];
            });
        });
    });

    /* Button choose timetable modal */

    $('#classModal').on('shown.bs.modal', function() {

        var dow = $('#dow').val();
        var nol = $('#nol').val();

        console.log(nol);

        for (i = 0; i < nol-1; i++) {
            var div = $('#lesson1');
            var clone = div.clone(true); // true means clone all childNodes and all event handlers
            var idDiv = "lesson" + (i + 2).toString();
            var htmlDiv = '<div id="'+idDiv+ '" class="form-group">' + clone.html() + "</div>";
            $('.timetable-lessons').append(htmlDiv);
        }
    });

    $('#btn-createTimetable').on('click', function(){
        var day = $('#dow').val();
        var selects = $('.timetable-lessons option:selected');
        for (i=0; i<selects.length;i++){
            var hour = selects[i].value;

            $('<input>').attr({
                type: 'hidden',
                id: 'lesson'+ i.toString(),
                name: 'lesson' + i.toString(),
                value: day.toString() + " " + hour.toString()
            }).appendTo('#add-lessons-form');
        }

        $("#btn-choose-timetable").attr('disabled', 'disabled');
        $('#classModal').modal('hide');
    });
});

