$(document).ready(function(){
    $('#footer').css('position', 'absolute');
    $('#select-lesson').change(function(){
        var lesson = $('#select-lesson').val();
        showForm(lesson);
    });
});

function showForm(lesson){
    if(lesson == 0){
        $('#edit-lessons').css('visibility', 'hidden');
        $('#footer').css('position', 'absolute');
    }else{
        $('#edit-lessons').css('visibility', 'visible');
        $.ajax({
            url: 'edit',
            type: 'POST',
            data: {'lesson': lesson},
            success: function(response){
                $('#edit-lessons').html(response);
                $('#footer').css('position', 'relative');
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#footer').css('position', 'absolute');
                console.log(jqXHR);
            }
        });
    }
}

function selectAll(ele){
    var checkboxes = document.getElementsByTagName('input');
    if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = true;
            }
        }
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
            console.log(i)
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
            }
        }
    }
}