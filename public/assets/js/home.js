$(document).ready(function () {

    //Calendario fecha nacimiento

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('#calendar').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        changeYear: true,
        yearRange: "-70:-16",
        defaultDate: '01/01/1999'
    });

    //Toma el nombre de la imagen y la coloca en el input

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });


    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    $('#link_login').on('click', function(e){
        $('#modal_login').modal('toggle');

    });

    $('#link_sign_up').on('click', function(e){
        $('#modal_sign_up').modal('toggle');

    });

    $('.dropdown-toggle').dropdown();
    $('#language_bar_chooser').dropdown();

    $('#facebook_button_sign_up').on('click', function(){
        $('#sign_up_form').submit();
    });

    x = $(document).height(); // +20 gives space between div and footer
    y = $(window).height();

    if (x<=y+10){ // 100 is the height of your footer
        $('#footer').css('position', 'absolute');
        //$('#footer').css('position', 'fixed');
        //$('#footer').css('display', 'block');
    }else{
        $('#footer').css('position', 'relative');
    }
});