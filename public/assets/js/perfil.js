$(document).ready(function(){

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('#date-call').datepicker({ dateFormat: 'dd-mm-yy' });

    var rating = $("#reputation").val();
    var latitude = parseFloat($("#latitude").val());
    var longitude = parseFloat($("#longitude").val());
    var i;

    var map;
    var myCenter=new google.maps.LatLng(latitude, longitude);
    var marker=new google.maps.Marker({
        position:myCenter
    });

    function initialize() {
        var mapProp = {
            center:myCenter,
            zoom: 18,
            draggable: false,
            scrollwheel: false,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
        marker.setMap(map);

        google.maps.event.addListener(marker, 'click', function() {

            infowindow.setContent(contentString);
            infowindow.open(map, marker);

        });
    };
    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addDomListener(window, "resize", resizingMap());

    function resizeMap() {
        if(typeof map =="undefined") return;
        setTimeout( function(){resizingMap();} , 400);
    }

    function resizingMap() {
        if(typeof map =="undefined") return;
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    }

    for(i=0; i<rating;i++){
        $("#rating-stars").append("<span class='glyphicon glyphicon-star-empty'></span>");
    }
    $("#rating-stars").append(rating.toString() + "/5");

    $('#map-modal').on('show.bs.modal', function() {
        //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
        resizeMap();
    });

    $(function() {
        $( "#slider-range-comment" ).slider({
            range: true,
            min: 0,
            max: 5,
            values: [ 0, 5 ],
            slide: function( event, ui ) {
                $( "#amount-comment" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-comment" ).val($( "#slider-range-comment" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-comment" ).slider( "values", 1 ) );

        var startPos = '$("#slider-comment").slider("value");';
        var endPos = '';

        $("#slider-range-comment").on("slidestop", function(event, ui) {
            endPos = ui.value;

            if (startPos != endPos) {
                var pathname = window.location.pathname.split("/");
                var supplier = pathname[pathname.length-1];
                ajaxSearchComment(supplier);
            }

            startPos = endPos;
        });
    });
});

function ajaxSearchComment(supplier){
    var values = {};
    values['option-sex'] = $('#option-sex').val();
    values['reputation'] = $('#amount-comment').val();
    values['supplier'] = supplier;
    values['labels'] = $('#tags').val();

    $.ajax({
        url: 'ajaxSearchComment',
        type: 'POST',
        data: values,
        success: function(response){
            $('#comments-list').html(response);
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.error(textStatus);
        }
    });

    return false;
}