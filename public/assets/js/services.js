$(document).ready(function(){
    $('#footer').css('position', 'absolute');
    $('#select-service').change(function(){
        var service = $('#select-service').val();
        showForm(service);
    });
});

function showForm(service){
    if(service == 0){
        $('#edit-services').css('visibility', 'hidden');
        $('#footer').css('position', 'absolute');
    }else{
        $('#edit-services').css('visibility', 'visible');
        $.ajax({
            url: 'edit',
            type: 'POST',
            data: {'service': service},
            success: function(response){
                $('#edit-services').html(response);
                $('#footer').css('position', 'relative');
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#footer').css('position', 'absolute');
                console.log(jqXHR);
            }
        });
    }
}

function selectAll(ele){
    var checkboxes = document.getElementsByTagName('input');
    if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = true;
            }
        }
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
            console.log(i)
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
            }
        }
    }
}